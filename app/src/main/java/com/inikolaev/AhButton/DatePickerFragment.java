package com.inikolaev.AhButton;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class DatePickerFragment extends DialogFragment {

	private OnDateSetListener callback;
	
	private int year;
	private int month;
	private int day;
	
	public static DatePickerFragment newInstance() {
		return new DatePickerFragment();
	}
	
	public void setCallback(OnDateSetListener listener) {
		callback = listener;
	}
	
	@Override
	public void setArguments(Bundle args) {
		super.setArguments(args);
		
		year = args.getInt("year", 0);
		month = args.getInt("month", 0);
		day = args.getInt("day", 0);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new DatePickerDialog(getActivity(), callback, year, month, day);
	}
}