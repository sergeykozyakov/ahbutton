package com.inikolaev.AhButton;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

public class GuardedPlacesActivity extends FragmentListActivity implements AsyncTaskFragment.AsyncTaskCallbacks {
	
	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private GuardedPlacesAdapter mGuardedPlacesAdapter;
	
	private ArrayList<String> mPlaceCities;
	private ArrayList<String> mPlaceStreets;
	private ArrayList<String> mPlaceHouses;
	private ArrayList<String> mPlaceBuildings;
	private ArrayList<String> mPlaceApartments;
	private ArrayList<String> mPlaceLabels;
	
	private Button mButtonGuardedPlacesNext;
	private Button mButtonGuardedPlacesAdd;
	
	// Приёмник события закрытия мастера
	private final BroadcastReceiver mWizardReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_WIZARD_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Сохранить"
	private final OnClickListener mButtonGuardedPlacesNextClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (MyDevice.hasInternetConnection(getApplicationContext())) {
				HashMap<String, String> params = new HashMap<String, String>();
				
				params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
				params.put("imei", MyDevice.getImei(getApplicationContext()));
				
				for (int i = 0; i < mGuardedPlacesAdapter.getCount(); i++) {
					params.put("city[" + i +"]", mGuardedPlacesAdapter.getItem(i).city);
					params.put("street[" + i +"]", mGuardedPlacesAdapter.getItem(i).street);
					params.put("house[" + i +"]", mGuardedPlacesAdapter.getItem(i).house);
					params.put("building[" + i +"]", mGuardedPlacesAdapter.getItem(i).building);
					params.put("apartment[" + i +"]", mGuardedPlacesAdapter.getItem(i).apartment);
					params.put("label[" + i +"]", mGuardedPlacesAdapter.getItem(i).label);
				}
				
				mAsyncTaskFragment.execute("user_places_set", params, null);
			}
			else {
				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Добавить запись"
	private final OnClickListener mButtonGuardedPlacesAddClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			showEditDialog(-1);
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				mProgressDialog.dismiss();
				
				finish();
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
			}
		}
		else {
			mProgressDialog.dismiss();
			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_guarded_places);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        // Регистрируем приёмник события закрытия
        registerReceiver(mWizardReceiver, new IntentFilter(AlarmService.ACTION_WIZARD_TERMINATED));
        
        // Кнопка "Сохранить"
	    mButtonGuardedPlacesNext = (Button)findViewById(R.id.buttonGuardedPlacesNext);
	    mButtonGuardedPlacesNext.setOnClickListener(mButtonGuardedPlacesNextClick);
	    
	    // Кнопка "Добавить запись"
	    mButtonGuardedPlacesAdd = (Button)findViewById(R.id.buttonGuardedPlacesAdd);
	    mButtonGuardedPlacesAdd.setOnClickListener(mButtonGuardedPlacesAddClick);
	    
	    // Заполняем список доверенных лиц
	    Intent intent = getIntent();
	    
	    mPlaceCities = intent.getStringArrayListExtra("place_cities");
	    if (mPlaceCities == null) {
	    	mPlaceCities = new ArrayList<String>();
	    }
	    
	    mPlaceStreets = intent.getStringArrayListExtra("place_streets");
	    if (mPlaceStreets == null) {
	    	mPlaceStreets = new ArrayList<String>();
	    }
	    
	    mPlaceHouses = intent.getStringArrayListExtra("place_houses");
	    if (mPlaceHouses == null) {
	    	mPlaceHouses = new ArrayList<String>();
	    }
	    
	    mPlaceBuildings = intent.getStringArrayListExtra("place_buildings");
	    if (mPlaceBuildings == null) {
	    	mPlaceBuildings = new ArrayList<String>();
	    }
	    
	    mPlaceApartments = intent.getStringArrayListExtra("place_apartments");
	    if (mPlaceApartments == null) {
	    	mPlaceApartments = new ArrayList<String>();
	    }
	    
	    mPlaceLabels = intent.getStringArrayListExtra("place_labels");
	    if (mPlaceLabels == null) {
	    	mPlaceLabels = new ArrayList<String>();
	    }
	    
	    ArrayList<GuardedPlace> places = new ArrayList<GuardedPlace>();
	    
	    for (int i = 0; i < mPlaceCities.size(); i++) {
	    	places.add(new GuardedPlace(mPlaceCities.get(i), mPlaceStreets.get(i), mPlaceHouses.get(i), mPlaceBuildings.get(i),	mPlaceApartments.get(i), mPlaceLabels.get(i)));
	    }
	    
	    mGuardedPlacesAdapter = new GuardedPlacesAdapter(this, R.layout.list_guarded_place, places);
	    setListAdapter(mGuardedPlacesAdapter);
	}
	
	@Override
	protected int getSupportLayoutResourceId() {
		return R.layout.activity_guarded_places;
	}
	
	@Override
    protected void onListItemClick(ListView l, View v, final int position, long id) {
		showEditDialog(position);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмники
		unregisterReceiver(mWizardReceiver);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
	
	// Показ диалога редактирования
	@SuppressLint("InflateParams")
	private void showEditDialog(final int position) {
		ScrollView view = (ScrollView)getLayoutInflater().inflate(R.layout.edit_guarded_place, null);
		final EditText city = (EditText)view.findViewById(R.id.editGuardedPlaceCity);
		final EditText street = (EditText)view.findViewById(R.id.editGuardedPlaceStreet);
		final EditText house = (EditText)view.findViewById(R.id.editGuardedPlaceHouse);
		final EditText building = (EditText)view.findViewById(R.id.editGuardedPlaceBuilding);
		final EditText apartment = (EditText)view.findViewById(R.id.editGuardedPlaceApartment);
		final EditText label = (EditText)view.findViewById(R.id.editGuardedPlaceLabel);
		
		if (position > -1) {
			city.setText(mGuardedPlacesAdapter.getItem(position).city);
			street.setText(mGuardedPlacesAdapter.getItem(position).street);
			house.setText(mGuardedPlacesAdapter.getItem(position).house);
			building.setText(mGuardedPlacesAdapter.getItem(position).building);
			apartment.setText(mGuardedPlacesAdapter.getItem(position).apartment);
			label.setText(mGuardedPlacesAdapter.getItem(position).label);
		}
		
		final AlertDialog editDialog = new AlertDialog.Builder(GuardedPlacesActivity.this)
    	.setMessage("Введите данные о месте пребывания")
    	.setCancelable(true)
    	.setView(view)
    	.setPositiveButton("Сохранить", null)
    	.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int id) {
    			dialog.dismiss();
    		}
    	})
    	.create();
	
		editDialog.show();
		
		editDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
    			if (city.length() == 0 || street.length() == 0 || house.length() == 0 || label.length() == 0) {
    				Toast.makeText(getApplicationContext(), "Заполните все поля, отмеченные звёздочкой!", Toast.LENGTH_SHORT).show();
    				return;
    			}
    			
    			if (position == -1) {
    				mGuardedPlacesAdapter.add(new GuardedPlace(city.getText().toString(), street.getText().toString(), house.getText().toString(),
    					building.getText().toString(), apartment.getText().toString(), label.getText().toString()));
    			}
    			else {
    				mGuardedPlacesAdapter.remove(mGuardedPlacesAdapter.getItem(position));
    				mGuardedPlacesAdapter.insert(new GuardedPlace(city.getText().toString(), street.getText().toString(), house.getText().toString(),
        				building.getText().toString(), apartment.getText().toString(), label.getText().toString()), position);
    			}
    			
    			editDialog.dismiss();
			}
		});
	}
}
