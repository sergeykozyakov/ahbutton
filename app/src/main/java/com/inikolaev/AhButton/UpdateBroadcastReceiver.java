package com.inikolaev.AhButton;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class UpdateBroadcastReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getData().getSchemeSpecificPart().equals(context.getPackageName())) {
			context.startService(new Intent(context, AlarmService.class));
		}
	}
}