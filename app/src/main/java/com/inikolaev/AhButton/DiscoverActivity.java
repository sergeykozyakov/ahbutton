package com.inikolaev.AhButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class DiscoverActivity extends FragmentListActivity implements AsyncTaskFragment.AsyncTaskCallbacks {
	
	private static final int REQUEST_ENABLE_BT = 1;
	private static final long LE_SCAN_PERIOD = 10000;
	
	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothDevicesAdapter mBluetoothDevicesAdapter;
	
	private ProgressBar mProgressDiscovery;
	private Button mButtonStartDiscovery;
	
	private ArrayList<String> mMacList;
	private Handler mHandler;
	
	private boolean mIsReconnect = false;
	private boolean mIsLeScanning = false;
	
	private boolean mReScan = false;
	
	// Приёмник события включения Bluetooth
	private final BroadcastReceiver mBluetoothStateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
			
			if (state == BluetoothAdapter.STATE_ON) {				
				scanBluetoothDevices();
			}
			else if (state == BluetoothAdapter.STATE_OFF) {				
				mProgressDiscovery.setVisibility(View.GONE);
	        	mButtonStartDiscovery.setVisibility(View.VISIBLE);
				
	        	mIsLeScanning = false;
				mReScan = false;
			}
	    }
	};
	
	// Приёмник событий начала/окончания поиска, нахождения устройства Bluetooth
	private final BroadcastReceiver mDeviceDiscoveryReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
	        String action = intent.getAction();
	        
	        if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
	        	mBluetoothDevicesAdapter.clear();
	        	mMacList.clear();
	        	
	        	mProgressDiscovery.setVisibility(View.VISIBLE);
	        	mButtonStartDiscovery.setVisibility(View.GONE);
	        	
	        	Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
	            if (pairedDevices.size() > 0) {
	            	for (BluetoothDevice device : pairedDevices) {
	            		addBluetoothDevice(device);
	            	}
	            }
	        }
	        else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
	            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	            addBluetoothDevice(device);
	        }
	        else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
	        	mProgressDiscovery.setVisibility(View.GONE);
	        	mButtonStartDiscovery.setVisibility(View.VISIBLE);
	        	
	        	if (mReScan) {
	        		mBluetoothAdapter.startDiscovery();
	        		mReScan = false;
	        	}
	        	else {
	        		if (MyDevice.hasBluetoothLe(getApplicationContext())) {
	        			scanLeDevices(true);
	        		}
	        	}
	        }
	    }
	};
	
	// Приёмник события установки/отказа подключения устройства
	private final BroadcastReceiver mConnectReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_CONNECT_OK_RESPONSED.equals(action)) {
				mProgressDialog.dismiss();
		    	finish();
			}			
			else if (AlarmService.ACTION_CONNECT_FAIL_RESPONSED.equals(action)) {
				mProgressDialog.dismiss();
			}
		}
	};
	
	// Приёмник события закрытия мастера
	private final BroadcastReceiver mWizardReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_WIZARD_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Поиск устройств"
	private final OnClickListener mButtonRepeatDiscoveryClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			testBluetooth();
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				Editor editor = mSettings.edit();
				editor.putInt(MyDevice.APP_PREFS_MODE_CONFS, http.getInt("mode_confs"));
				editor.putString(MyDevice.APP_PREFS_LABEL_1, http.getString("label", new String[] {"buttons", "button"}));
				editor.putInt(MyDevice.APP_PREFS_CONFIRMATION_1, http.getInt("confirmation", new String[] {"buttons", "button"}));
				editor.putString(MyDevice.APP_PREFS_LABEL_2, http.getString("label", new String[] {"buttons", "button_1"}));
				editor.putInt(MyDevice.APP_PREFS_CONFIRMATION_2, http.getInt("confirmation", new String[] {"buttons", "button_1"}));
				editor.putString(MyDevice.APP_PREFS_MAC, http.getParam("mac"));
				editor.putBoolean(MyDevice.APP_PREFS_NO_DEVICE, false);
				editor.apply();
				
				// Запуск/перезапуск службы AlarmService
				if (MyDevice.isAlarmServiceRunning(this)) {
					stopService(new Intent(this, AlarmService.class));
				}
				
				Intent intent = new Intent(this, AlarmService.class);
				intent.putExtra("no_silent", true);
				
				if (mIsReconnect) {
					if (mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {					
						intent.putExtra("key_sets", true);
					}
				}
				else {
					intent.putExtra("key_sets", true);
				}
				
				startService(intent);
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
			}
		}
		else {
			mProgressDialog.dismiss();
			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
		}
	}
	
    
	
    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover);
        
        if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
        
        // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        // Регистрируем приёмники событий
        registerReceiver(mBluetoothStateReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        
        IntentFilter deviceFilter = new IntentFilter();
        deviceFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        deviceFilter.addAction(BluetoothDevice.ACTION_FOUND);
        deviceFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mDeviceDiscoveryReceiver, deviceFilter);
        
        IntentFilter connectFilter = new IntentFilter();
        connectFilter.addAction(AlarmService.ACTION_CONNECT_OK_RESPONSED);
        connectFilter.addAction(AlarmService.ACTION_CONNECT_FAIL_RESPONSED);
        registerReceiver(mConnectReceiver, connectFilter);
        
        registerReceiver(mWizardReceiver, new IntentFilter(AlarmService.ACTION_WIZARD_TERMINATED));
        
        // Инициализируем элементы
        mProgressDiscovery = (ProgressBar)findViewById(R.id.progressDiscovery);
        
        mButtonStartDiscovery = (Button)findViewById(R.id.buttonStartDiscovery);
        mButtonStartDiscovery.setOnClickListener(mButtonRepeatDiscoveryClick);
        
        mBluetoothDevicesAdapter = new BluetoothDevicesAdapter(this, android.R.layout.simple_list_item_2, new ArrayList<MyBluetoothDevice>());
        setListAdapter(mBluetoothDevicesAdapter);
        
        mMacList = new ArrayList<String>();
        mHandler = new Handler();
        
        // Получаем параметр подключения
        mIsReconnect = getIntent().getBooleanExtra("is_reconnect", false);
    }
    
    @Override
	protected int getSupportLayoutResourceId() {
		return R.layout.activity_discover;
	}
    
	@Override
	public void onPause() {
		super.onPause();
		
		if (mBluetoothAdapter != null) {
			if (mBluetoothAdapter.isDiscovering()) {
    			mBluetoothAdapter.cancelDiscovery();
        	}
			
			if (mIsLeScanning) {
				scanLeDevices(false);
			}
		}
	}
	
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
		// Запрашиваем настройки кнопок с сервера
		if (MyDevice.hasInternetConnection(this)) {
			HashMap<String, String> params = new HashMap<String, String>();
						
			params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
			params.put("imei", MyDevice.getImei(this));
			params.put("mac", mMacList.get(position));
						
			mAsyncTaskFragment.execute("user_modes_get", params, null);
		}
		else {
			Toast.makeText(this, "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	
    	// Отключаем приёмники
    	unregisterReceiver(mBluetoothStateReceiver);
    	unregisterReceiver(mDeviceDiscoveryReceiver);
    	unregisterReceiver(mConnectReceiver);
    	unregisterReceiver(mWizardReceiver);
    }
    
    
    
	// Проверка Bluetooth
    private void testBluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        
        if (mBluetoothAdapter == null) {
        	Toast.makeText(this, "Bluetooth не поддерживается на вашем устройстве!", Toast.LENGTH_SHORT).show();
        	return;
        }
        
        if (!mBluetoothAdapter.isEnabled()) {
        	startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), REQUEST_ENABLE_BT);
        }
        else {
        	scanBluetoothDevices();
        }
    }
    
    // Поиск устройств Bluetooth
    private void scanBluetoothDevices() {
    	if (mBluetoothAdapter.isDiscovering()) {
    		mReScan = true;
    		mBluetoothAdapter.cancelDiscovery();
        }
    	else {
    		mBluetoothAdapter.startDiscovery();
    	}
    }
    
    // Поиск устройств Bluetooth Low Energy
    @SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	private void scanLeDevices(boolean enable) {
    	final BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
    		@Override
    		public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
    			runOnUiThread(new Runnable() {
    				@Override
    				public void run() {
    					addBluetoothDevice(device);
    				}
    			});
    		}
    	};
    	
    	if (enable) {
    		mHandler.postDelayed(new Runnable() {
    			@Override
    			public void run() {
    				mProgressDiscovery.setVisibility(View.GONE);
    	        	mButtonStartDiscovery.setVisibility(View.VISIBLE);
    				
    				mIsLeScanning = false;
    				mBluetoothAdapter.stopLeScan(leScanCallback);
    			}
    		}, LE_SCAN_PERIOD);
    		
    		mProgressDiscovery.setVisibility(View.VISIBLE);
        	mButtonStartDiscovery.setVisibility(View.GONE);
    		
    		mIsLeScanning = true;
    		mBluetoothAdapter.startLeScan(leScanCallback);
    	}
    	else {
    		mProgressDiscovery.setVisibility(View.GONE);
        	mButtonStartDiscovery.setVisibility(View.VISIBLE);
    		
    		mIsLeScanning = false;
    		mBluetoothAdapter.stopLeScan(leScanCallback);
    	}
    }
    
    // Добавление нового Bluetooth-устройства в список
    @SuppressLint("NewApi")
	private void addBluetoothDevice(BluetoothDevice device) {
    	int index = mMacList.indexOf(device.getAddress());

    	int deviceType = 0;
    	String deviceName = device.getName();
    	String deviceTypeText = "";
        
    	if (deviceName == null) {
    		deviceName = device.getAddress();
    	}

    	if (Build.VERSION.SDK_INT >= 18) {
        	deviceType = device.getType();
        	
        	switch (deviceType) {
        		case 2:
        			deviceTypeText = " (LE)";
        			break;
        		case 3:
        			deviceTypeText = " (BR/EDR/LE)";
        			break;
        	}
        }
        
    	if (index == -1) {
    		mBluetoothDevicesAdapter.add(new MyBluetoothDevice(deviceName, device.getAddress() + deviceTypeText, deviceType));
    		mMacList.add(device.getAddress());
    	}
    	else {
    		mBluetoothDevicesAdapter.remove(mBluetoothDevicesAdapter.getItem(index));
    		mBluetoothDevicesAdapter.insert(new MyBluetoothDevice(deviceName, device.getAddress() + deviceTypeText, deviceType), index);
        }
    	
    	mBluetoothDevicesAdapter.notifyDataSetChanged();
    }
    
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}
