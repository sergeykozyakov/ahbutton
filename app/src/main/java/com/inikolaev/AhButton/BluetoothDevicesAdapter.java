package com.inikolaev.AhButton;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class BluetoothDevicesAdapter extends ArrayAdapter<MyBluetoothDevice> {
	private Context context; 
	private int layoutResourceId;    
	private ArrayList<MyBluetoothDevice> data = null;

	public BluetoothDevicesAdapter(Context context, int layoutResourceId, ArrayList<MyBluetoothDevice> data) {
		super(context, layoutResourceId, data);
		
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		View row = convertView;
		BluetoothDevicesHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new BluetoothDevicesHolder();
			holder.text1 = (TextView)row.findViewById(android.R.id.text1);
			holder.text2 = (TextView)row.findViewById(android.R.id.text2);

			row.setTag(holder);
		}
		else {
			holder = (BluetoothDevicesHolder)row.getTag();
		}

		MyBluetoothDevice device = data.get(position);
		holder.text1.setText(device.name);
		holder.text2.setText(device.address);

		return row;
	}

	private static class BluetoothDevicesHolder {
		TextView text1;
		TextView text2;
	}
}
