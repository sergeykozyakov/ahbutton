package com.inikolaev.AhButton;

import java.net.URLEncoder;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@SuppressWarnings("deprecation")
public class MyHttpClient {

	private String mServerUrl = "https://ahbutton.ru/api.php";
	
	private HttpClient mHttpClient;
	private HttpPost mHttpPost;
	
	private MultipartEntityBuilder mBuilder;
	private ProgressHttpEntityWrapper.ProgressCallback mUploadProgressCallback;
	
	private HttpResponse mResponse;
	private HttpEntity mResEntity;
	
	private DocumentBuilderFactory mDbf;
	private DocumentBuilder mDb;
	private Document mDocument;
	
	private boolean mIsSuccess;
	
	private HashMap<String, String> mParams;
	private HashMap<String, Object> mResp;
	
	// Инициализация HTTP-клиента
	private HttpClient getMyHttpClient() {
	    try {
	        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
	        trustStore.load(null, null);

	        SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
	        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

	        HttpParams params = new BasicHttpParams();
	        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
	        HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

	        SchemeRegistry registry = new SchemeRegistry();
	        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	        registry.register(new Scheme("https", sf, 443));

	        ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

	        return new DefaultHttpClient(ccm, params);
	    } 
	    catch (Exception e) {
	        return new DefaultHttpClient();
	    }
	}
	
	// Callback-функция прогресса загрузки
	public void setUploadProgressCallback(ProgressHttpEntityWrapper.ProgressCallback callback) {
		mUploadProgressCallback = callback;
	}
	
	// Обработка тэгов
	private HashMap<String, Object> parseXml(Node root) {
		HashMap<String, Object> entryMap = new HashMap<String, Object>();
		
		if (root != null) {
			NodeList nl = root.getChildNodes();
			int ind = 0;
			
			for (int i = 0; i < nl.getLength(); i++) {
				Node node = nl.item(i);
				
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element el = (Element)node;
					NodeList nlChild = el.getElementsByTagName("*");
					
					String nodeName = node.getNodeName();
					if (entryMap.containsKey(nodeName)) {
						nodeName += "_" + Integer.toString(ind);
					}
					
					if (nlChild.getLength() > 0) {
						entryMap.put(nodeName, parseXml(node));
					}
					else {					
						entryMap.put(nodeName, node.getTextContent());
					}
				ind++;
				}
			}
		}
		
		return entryMap;
	}
	
	public MyHttpClient() {
		mHttpClient = getMyHttpClient();
		mHttpPost = new HttpPost(mServerUrl);
		
		mIsSuccess = true;
		mResp = new HashMap<String, Object>();
	}
	
	// POST-запрос к серверу
	public void doPost(String command, HashMap<String, String> params, HashMap<String, byte[]> files) {
		try {	
			mBuilder = MultipartEntityBuilder.create();
			mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			
			mBuilder.addTextBody("command", command);
			
			mParams = params;
			if (mParams != null) {
				Iterator<String> keySetIterator = mParams.keySet().iterator();
				
				while (keySetIterator.hasNext()) {
					String key = keySetIterator.next();
					
					if (mParams.get(key) != null) {
						if (mParams.get(key).startsWith("_")) continue;
						
						mBuilder.addTextBody(key, URLEncoder.encode(mParams.get(key), "UTF-8"));
					}
				}
			}
			
			if (files != null) {
				Iterator<String> keySetIterator = files.keySet().iterator();
				
				while (keySetIterator.hasNext()) {
					String key = keySetIterator.next();
					
					if (files.get(key) != null) {
						mBuilder.addBinaryBody(key, files.get(key), ContentType.create("image/jpeg"), key + ".jpg");
					}
				}
			}
			
			if (mUploadProgressCallback != null) {
				mHttpPost.setEntity(new ProgressHttpEntityWrapper(mBuilder.build(), mUploadProgressCallback));
			}
			else {
				mHttpPost.setEntity(mBuilder.build());
			}
			
			mResponse = mHttpClient.execute(mHttpPost);
			mResEntity = mResponse.getEntity();
			
			if (mResEntity != null) {		    
				mDbf = DocumentBuilderFactory.newInstance();
				mDb = mDbf.newDocumentBuilder();
				
				mDocument = mDb.parse(mResEntity.getContent());
				Node response = mDocument.getElementsByTagName("response").item(0);
				
				mResp = parseXml(response);
				mIsSuccess = true;
			}
		}
		catch (Exception e) {
			mIsSuccess = false;
		}
	}
	
	// Успешно ли выполнен запрос
	public boolean isSuccess() {
		return mIsSuccess;
	}
	
	// Чтение строкового ответа сервера
	@SuppressWarnings("unchecked")
	public String getString(String key, String[] routes) {
		String value = "";
		HashMap<String, Object> map = mResp;
		
		try {
			if (routes != null) {
				for (String route : routes) {
					map = (HashMap<String, Object>)map.get(route);
				}
			}
			value = (String)map.get(key);
			if (value == null) value = "";
		} catch (Exception e) {}
		
		return value;
	}
	
	public String getString(String key) {
		return getString(key, null);
	}
	
	// Чтение числового ответа сервера
	@SuppressWarnings("unchecked")
	public int getInt(String key, String[] routes) {
		int value = 0;
		HashMap<String, Object> map = mResp;
		
		try {
			if (routes != null) {
				for (String route : routes) {
					map = (HashMap<String, Object>)map.get(route);
				}
			}
			value = Integer.parseInt((String)map.get(key));
		} catch (Exception e) {}
		
		return value;
	}
	
	public int getInt(String key) {
		return getInt(key, null);
	}
	
	// Чтение дробного ответа сервера
	@SuppressWarnings("unchecked")
	public double getDouble(String key, String[] routes) {
		double value = 0.0;
		HashMap<String, Object> map = mResp;
		
		try {
			if (routes != null) {
				for (String route : routes) {
					map = (HashMap<String, Object>)map.get(route);
				}
			}
			value = Double.parseDouble((String)mResp.get(key));
		} catch (Exception e) {}
		
		return value;
	}
	
	public double getDouble(String key) {
		return getDouble(key, null);
	}
	
	// Получение списка тэгов в необработанном виде
	public HashMap<String, Object> getRespMap() {
		return mResp;
	}
	
	// Получение параметра по ключу
	public String getParam(String name) {
		String value = "";
		
		if (mParams.containsKey(name)) {
			value = mParams.get(name);
			if (value == null) value = "";
		}
		
		return value;
	}
}
