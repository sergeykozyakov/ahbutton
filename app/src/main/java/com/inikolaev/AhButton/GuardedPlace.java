package com.inikolaev.AhButton;

import android.os.Parcel;
import android.os.Parcelable;

public class GuardedPlace implements Parcelable {
	public String city;
	public String street;
	public String house;
	public String building;
	public String apartment;
	public String label;
	
	public GuardedPlace(String city, String street, String house, String building, String apartment, String label) {
		this.city = city;
		this.street = street;
		this.house = house;
		this.building = building;
		this.apartment = apartment;
		this.label = label;
	}
	
	public GuardedPlace(Parcel in) {
		String[] data = new String[6];
		in.readStringArray(data);
		
		this.city = data[0];
		this.street = data[1];
		this.house = data[2];
		this.building = data[3];
		this.apartment = data[4];
		this.label = data[5];
    }

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStringArray(new String[] {this.city, this.street, this.house, this.building, this.apartment, this.label});
	}
	
	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public GuardedPlace createFromParcel(Parcel in) {
			return new GuardedPlace(in); 
		}

		public GuardedPlace[] newArray(int size) {
			return new GuardedPlace[size];
		}
	};
}