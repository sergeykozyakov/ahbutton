package com.inikolaev.AhButton;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

public class MyDevice {
	
	public static final String APP_PREFS = "settings";
	
	public static final String APP_PREFS_REGISTERING = "registering";
	public static final String APP_PREFS_LIC = "lic";
	
	public static final String APP_PREFS_LOGIN = "login";
	public static final String APP_PREFS_PASSWORD = "password";
	
	public static final String APP_PREFS_MAC = "mac";
	public static final String APP_PREFS_NO_DEVICE = "no_device";
	
	public static final String APP_PREFS_ATTEMPTS = "attempts";
	
	public static final String APP_PREFS_MODE_CONFS = "mode_confs";
	
	public static final String APP_PREFS_CONFIRMATION_1 = "confirmation_1";
	public static final String APP_PREFS_CONFIRMATION_2 = "confirmation_2";
	
	public static final String APP_PREFS_LABEL_1 = "label_1";
	public static final String APP_PREFS_LABEL_2 = "label_2";
	
	public static final String APP_PREFS_TARIFF_SET = "tariff_set";
	
	// Проверяем доступность интернета
	public static boolean hasInternetConnection(Context context) {
	    ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    if (cm == null) {
	    	return false;
	    }
	    
	    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
	    if (netInfo == null) {
	    	return false;
	    }
	    
	    int connects = 0;	    
	    for (NetworkInfo ni : netInfo) {
	        if (ni.getType() == ConnectivityManager.TYPE_BLUETOOTH) {
	        	continue;
	        }
	        
	        if (ni.isConnected()) {
	            connects++;
	        }
	    }
	    
	    if (connects > 0) {
	    	return true;
	    }
	    
		return false;
	}
	
	// Проверяем наличие Bluetooth Low Energy
	@SuppressLint("InlinedApi")
	public static boolean hasBluetoothLe(Context context) {
		if (Build.VERSION.SDK_INT >= 18) {
			return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
		}
		
		return false;
	}
	
	// Включен ли авиарежим
	@SuppressWarnings("deprecation")
	public static boolean isAirplaneModeOn(Context context) {
		return Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) != 0;
	}
	
	// Получаем IMEI-код устройства
	public static String getImei(Context context) {
		TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		String imei = tm.getDeviceId();
		
		if (imei == null) {
			return "004999010640000";
		}
		
	    return imei;
	}
	
	// Запущена ли служба
	public static boolean isAlarmServiceRunning(Context context) {
	    ActivityManager manager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
	    
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (AlarmService.class.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    
	    return false;
	}
	
	// Получаем размеры экрана
	@SuppressLint("NewApi")
	public static int[] getScreenSizes(Context context) {
	    int screenWidth = 0;
	    int screenHeight = 0;
	    
	    Point size = new Point();
	    try {
	    	((Activity)context).getWindowManager().getDefaultDisplay().getRealSize(size);
	    	screenWidth = size.x;
	    	screenHeight = size.y;
	    }
	    catch (NoSuchMethodError e) {
	    	DisplayMetrics metrics = new DisplayMetrics();
	    	((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
		    screenWidth = metrics.widthPixels;
		    screenHeight = metrics.heightPixels;
	    }
	    
	    return new int[] {screenWidth, screenHeight};
	}
	
	// Перевод dp в пиксели
	public static int dpToPixels(Context context, float dp) {
	    final float scale = context.getResources().getDisplayMetrics().density;
	    return (int)(dp * scale + 0.5f);
	}
}
