package com.inikolaev.AhButton;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class TrustedPersonsAdapter extends ArrayAdapter<TrustedPerson> {
	private Context context; 
	private int layoutResourceId;    
	private ArrayList<TrustedPerson> data = null;

	public TrustedPersonsAdapter(Context context, int layoutResourceId, ArrayList<TrustedPerson> data) {
		super(context, layoutResourceId, data);
		
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		View row = convertView;
		TrustedPersonsHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new TrustedPersonsHolder();
			holder.textFullName = (TextView)row.findViewById(R.id.textTrustedFullName);
			holder.textContactPhone = (TextView)row.findViewById(R.id.textTrustedContactPhone);
			holder.buttonDelete = (Button)row.findViewById(R.id.buttonTrustedDelete);

			row.setTag(holder);
		}
		else {
			holder = (TrustedPersonsHolder)row.getTag();
		}

		TrustedPerson person = data.get(position);
		holder.textFullName.setText(person.lastName + " " + person.firstName + " " + person.patrName);
		holder.textContactPhone.setText(person.contactPhone);
		holder.buttonDelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog exitDialog = new AlertDialog.Builder(context)
            	.setMessage("Вы подтверждаете удаление?")
            	.setCancelable(true)
            	.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            		public void onClick(DialogInterface dialog, int id) {
                    	dialog.dismiss();
                    	
                    	data.remove(position);
        				notifyDataSetChanged();
                	}
            	})
            	.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            		public void onClick(DialogInterface dialog, int id) {
            			dialog.dismiss();
            		}
            	})
            	.create();
			
    			exitDialog.show();
			}			
		});

		return row;
	}
	
	public ArrayList<TrustedPerson> getItems() {
		return data;
	}

	private static class TrustedPersonsHolder {
		TextView textFullName;
		TextView textContactPhone;
		Button buttonDelete;
	}
}
