package com.inikolaev.AhButton;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class GuardedPlacesAdapter extends ArrayAdapter<GuardedPlace> {
	private Context context; 
	private int layoutResourceId;    
	private ArrayList<GuardedPlace> data;
	
	public GuardedPlacesAdapter(Context context, int layoutResourceId, ArrayList<GuardedPlace> data) {
		super(context, layoutResourceId, data);
		
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}
	
	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		View row = convertView;
		GuardedPlacesHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new GuardedPlacesHolder();
			holder.textLabel = (TextView)row.findViewById(R.id.textGuardedPlaceLabel);
			holder.textAddress = (TextView)row.findViewById(R.id.textGuardedPlaceAddress);
			holder.buttonDelete = (Button)row.findViewById(R.id.buttonGuardedPlaceDelete);

			row.setTag(holder);
		}
		else {
			holder = (GuardedPlacesHolder)row.getTag();
		}
		
		GuardedPlace place = data.get(position);
		
		String building = place.building.length() > 0 ? "/" + place.building : ""; 
		String apartment = place.apartment.length() > 0 ? " кв/оф " + place.apartment : "";

		holder.textLabel.setText(place.label);
		holder.textAddress.setText(place.city + ", " + place.street + ", " + place.house + building + apartment);
		holder.buttonDelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog exitDialog = new AlertDialog.Builder(context)
            	.setMessage("Вы подтверждаете удаление?")
            	.setCancelable(true)
            	.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            		public void onClick(DialogInterface dialog, int id) {
                    	dialog.dismiss();
                    	
                    	data.remove(position);
        				notifyDataSetChanged();
                	}
            	})
            	.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            		public void onClick(DialogInterface dialog, int id) {
            			dialog.dismiss();
            		}
            	})
            	.create();
			
    			exitDialog.show();
			}			
		});

		return row;
	}
	
	public ArrayList<GuardedPlace> getItems() {
		return data;
	}
	
	private static class GuardedPlacesHolder {
		TextView textLabel;
		TextView textAddress;
		Button buttonDelete;
	}
}
