package com.inikolaev.AhButton;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PayActivity extends Activity {

	private SharedPreferences mSettings;
	
	private TextView mTextPayLs;
	private TextView mTextPayStatus;
	private TextView mTextPayBalance;
	private EditText mEditPaySum;
	private Button mButtonPayNext;
	
	// Приёмник события закрытия окна
	private final BroadcastReceiver mStopReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_APP_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Оплатить"
	private final OnClickListener mButtonPayNextClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String login = mSettings.getString(MyDevice.APP_PREFS_LOGIN, "");
			String password = mSettings.getString(MyDevice.APP_PREFS_PASSWORD, "");
			String cost = mEditPaySum.getText().toString();
			
			if (cost.equals("")) {
				Toast.makeText(getApplicationContext(), "Укажите сумму к оплате!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://ahbutton.ru/saver.php?menu=pay&login=" + login + "&password=" + password + "&cost=" + cost)));
		}
	};
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_pay);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
	    // Регистрируем приёмник события закрытия окна
	    registerReceiver(mStopReceiver, new IntentFilter(AlarmService.ACTION_APP_TERMINATED));
        
        // Текстовые поля
	    mTextPayLs = (TextView)findViewById(R.id.textPayLs);
	    mTextPayStatus = (TextView)findViewById(R.id.textPayStatus);
        mTextPayBalance = (TextView)findViewById(R.id.textPayBalance);
        
        // Поле ввода суммы
        mEditPaySum = (EditText)findViewById(R.id.editPaySum);
        
        // Кнопка "Оплатить"
        mButtonPayNext = (Button)findViewById(R.id.buttonPayNext);
        mButtonPayNext.setOnClickListener(mButtonPayNextClick);
        
        // Заполнение значений
	    Intent intent = getIntent();
	    
	    String ls = intent.getStringExtra("ls");
	    if (ls != null) {
	    	mTextPayLs.setText(ls);
	    }
	    
	    String status = intent.getStringExtra("status");
	    if (status != null) {
	    	mTextPayStatus.setText(status);
	    }
	    
	    String balance = intent.getStringExtra("balance");
	    if (balance != null) {
	    	mTextPayBalance.setText(balance);
	    }
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмник
		unregisterReceiver(mStopReceiver);
	}
}
