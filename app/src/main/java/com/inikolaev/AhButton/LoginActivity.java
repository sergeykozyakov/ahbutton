package com.inikolaev.AhButton;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends FragmentActivity implements AsyncTaskFragment.AsyncTaskCallbacks {

	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private EditText mEditLogin;
	private EditText mEditPassword;
	private Button mButtonLoginNext;
	private Button mButtonRestorePassword;
	
	// Приёмник события закрытия мастера
	private final BroadcastReceiver mWizardReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_WIZARD_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Далее"
	private final OnClickListener mButtonLoginNextClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// Проверяем введённые данные
			if (mEditLogin.length() == 0) {
				Toast.makeText(getApplicationContext(), "Введите логин!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (mEditPassword.length() < 5) {
				Toast.makeText(getApplicationContext(), "Длина пароля должна быть не менее 5 символов!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// Логинимся на сервере
			if (MyDevice.hasInternetConnection(getApplicationContext())) {
				HashMap<String, String> params = new HashMap<String, String>();
								
				params.put("login", mEditLogin.getText().toString());
				params.put("password", mEditPassword.getText().toString());
				params.put("imei", MyDevice.getImei(getApplicationContext()));
				
				mAsyncTaskFragment.execute("user_login", params, null);
			}
			else {
				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Восстановить пароль"
	private final OnClickListener mButtonRestorePasswordClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://ahbutton.ru/recovery.php")));
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				Editor editor = mSettings.edit();
				editor.putString(MyDevice.APP_PREFS_LOGIN, mEditLogin.getText().toString());
				editor.putString(MyDevice.APP_PREFS_PASSWORD, mEditPassword.getText().toString());
				editor.apply();
				
				mProgressDialog.dismiss();
					        				
				Intent intent = new Intent(this, ProfileActivity.class);
				//intent.putExtra("last_name", http.getString("last_name"));
				//intent.putExtra("first_name", http.getString("first_name"));
				//intent.putExtra("patr_name", http.getString("patr_name"));
				//intent.putExtra("birth_date", http.getString("birth_date"));
				intent.putExtra("phone", http.getString("phone"));
				intent.putExtra("codeword", http.getString("codeword"));
				intent.putExtra("email", http.getString("email"));
				//intent.putExtra("city", http.getString("city"));
				//intent.putExtra("street", http.getString("street"));
				//intent.putExtra("house", http.getString("house"));
				//intent.putExtra("building", http.getString("building"));
				//intent.putExtra("apartment", http.getString("apartment"));
				//intent.putExtra("passport_ser_num", http.getString("passport_ser_num"));
				//intent.putExtra("passport_issued_by", http.getString("passport_issued_by"));
				//intent.putExtra("passport_issue_date", http.getString("passport_issue_date"));
				intent.putExtra("passport_photo_status", http.getInt("passport_photo_status"));
				intent.putExtra("passport_photo_url", http.getString("passport_photo_url"));
				intent.putExtra("passport_photo_reg_status", http.getInt("passport_photo_reg_status"));
				intent.putExtra("passport_photo_reg_url", http.getString("passport_photo_reg_url"));
				intent.putExtra("guarded_last_name", http.getString("guarded_last_name"));
				intent.putExtra("guarded_first_name", http.getString("guarded_first_name"));
				intent.putExtra("guarded_patr_name", http.getString("guarded_patr_name"));
				intent.putExtra("guarded_birth_date", http.getString("guarded_birth_date"));
				intent.putExtra("guarded_phone", http.getString("guarded_phone"));
				//intent.putExtra("guarded_city", http.getString("guarded_city"));
				//intent.putExtra("guarded_street", http.getString("guarded_street"));
				//intent.putExtra("guarded_house", http.getString("guarded_house"));
				//intent.putExtra("guarded_building", http.getString("guarded_building"));
				//intent.putExtra("guarded_apartment", http.getString("guarded_apartment"));
				intent.putExtra("guarded_phone_contact", http.getString("guarded_phone_contact"));
				intent.putExtra("guarded_deseases", http.getString("guarded_deseases"));
				intent.putExtra("guarded_photo_status", http.getInt("guarded_photo_status"));
				intent.putExtra("guarded_photo_url", http.getString("guarded_photo_url"));
				
				startActivity(intent);
				finish();
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
			}
		}
		else {
			mProgressDialog.dismiss();
			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_login);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    if (mAsyncTaskFragment.isTaskRunning()) {
	    	showProgressDialog();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        // Регистрируем приёмник события закрытия мастера
        registerReceiver(mWizardReceiver, new IntentFilter(AlarmService.ACTION_WIZARD_TERMINATED));
	    
	    // Поля ввода
        mEditLogin = (EditText)findViewById(R.id.editLogin);
        mEditPassword = (EditText)findViewById(R.id.editPassword);
	    
	    // Кнопка "Далее"
	    mButtonLoginNext = (Button)findViewById(R.id.buttonLoginNext);
	    mButtonLoginNext.setOnClickListener(mButtonLoginNextClick);
	    
	    // Кнопка "Восстановить пароль"
	    mButtonRestorePassword = (Button)findViewById(R.id.buttonRestorePassword);
	    mButtonRestorePassword.setOnClickListener(mButtonRestorePasswordClick);
	    
		// Автозаполнение логина/пароля
		if (!mSettings.getString(MyDevice.APP_PREFS_LOGIN, "").equals("") && !mSettings.getString(MyDevice.APP_PREFS_PASSWORD, "").equals("") &&
			mEditLogin.getText().toString().equals("") && mEditPassword.getText().toString().equals("") && savedInstanceState == null) {
			
			mEditLogin.setText(mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
			mEditPassword.setText(mSettings.getString(MyDevice.APP_PREFS_PASSWORD, ""));
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмник
		unregisterReceiver(mWizardReceiver);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}
