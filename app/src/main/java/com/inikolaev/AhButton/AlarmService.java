package com.inikolaev.AhButton;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class AlarmService extends Service {
	
	private static final int ALARM_MODE_DURATION = 120000;
	private static final int APP_SYNC_DURATION = 900000;
	private static final int APP_BATTERY_DURATION = 24*3600*1000;
	private static final int LOCATION_DURATION = 20000;
	
	public final static String ACTION_CONNECT_OK_RESPONSED = "com.inikolaev.AhButton.CONNECT_OK_RESPONSED";
	public final static String ACTION_CONNECT_FAIL_RESPONSED = "com.inikolaev.AhButton.CONNECT_FAIL_RESPONSED";
	
	public final static String ACTION_ALARM_STARTED = "com.inikolaev.AhButton.ALARM_STARTED";
	public final static String ACTION_ALARM_FINISHED = "com.inikolaev.AhButton.ALARM_FINISHED";
	
	public final static String ACTION_ALARM_REQUESTED = "com.inikolaev.AhButton.ALARM_REQUESTED";
	public final static String ACTION_ALARM_SENT = "com.inikolaev.AhButton.ALARM_SENT";
	public final static String ACTION_ALARM_CANCELED = "com.inikolaev.AhButton.ALARM_CANCELED";
	
	public final static String ACTION_TEST_STARTED = "com.inikolaev.AhButton.TEST_STARTED";
	public final static String ACTION_TEST_FINISHED = "com.inikolaev.AhButton.TEST_FINISHED";
	public final static String ACTION_TEST_SENT = "com.inikolaev.AhButton.TEST_SENT";
	
	public final static String ACTION_FINISH_BROADCASTED = "com.inikolaev.AhButton.FINISH_BROADCASTED";
	public final static String ACTION_SYNC_BROADCASTED = "com.inikolaev.AhButton.SYNC_BROADCASTED";
	public final static String ACTION_BATTERY_BROADCASTED = "com.inikolaev.AhButton.BATTERY_BROADCASTED";
	
	public final static String ACTION_WIZARD_TERMINATED = "com.inikolaev.AhButton.WIZARD_TERMINATED";
	public final static String ACTION_APP_TERMINATED = "com.inikolaev.AhButton.APP_TERMINATED";
	
	private static final int BLUETOOTH_RESPONSE = 1;
	private static final int SHOW_TOAST = 2;
	
	private SharedPreferences mSettings;
	private AlarmManager mAlarmManager;
	private Handler mHandler;
	
	private Object mBluetoothGattObj;
	
	private PendingIntent mAlarmPIntent;
	private PendingIntent mSyncPIntent;
	private PendingIntent mBatteryPIntent;
	
	private AlarmHttpTask mAlarmHttpTask;
	private GpsHttpTask mGpsHttpTask;
	private SyncHttpTask mSyncHttpTask;
	
	private BluetoothAdapter mBluetoothAdapter;
	private LocationManager mLocationManager;
	private NotificationManager mNotificationManager;
	
	private Vibrator mVibrator;
	private MediaPlayer mMediaPlayer;
	
	private ConnectBluetoothThread mConnectBluetoothThread;
	private IOBluetoothThread mIOBluetoothThread;
	
	private int mAlarmId = 0;
	private int mButtonId = 0;
	
	private String mLastBatteryChargeHex = "";
	
	private boolean mNoSilent = false;
	private boolean mKeySets = false;
	private boolean mIsTestMode = false;
	private boolean mIsSwitchingOff = false;
	
	// Приёмник события включения Bluetooth
	private final BroadcastReceiver mBluetoothStateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
			
			if (state == BluetoothAdapter.STATE_ON && !mSettings.getBoolean(MyDevice.APP_PREFS_NO_DEVICE, false)) {
				connectBluetoothDevice();
			}
	    }
	};
	
	// Приёмник событий AlarmService
	private final BroadcastReceiver mAlarmReceiver = new BroadcastReceiver() {
		@SuppressLint("NewApi")
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
	        
			if (ACTION_ALARM_REQUESTED.equals(action)) {
				if (mAlarmId > 0) {
					Intent i = new Intent(ACTION_ALARM_STARTED);
					i.putExtra("alarm_id", mAlarmId);
					i.putExtra("button_id", mButtonId);
					sendBroadcast(i);
				}
			}
			else if (ACTION_ALARM_SENT.equals(action)) {
	        	if (mAlarmId != 0 || mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {
	        		return;
	        	}
	        	
	        	mButtonId = intent.getIntExtra("button_id", 0);
	        	if (mButtonId > 0) {
	        		doAction();
	        	}
	        }
	        else if (ACTION_ALARM_CANCELED.equals(action)) {
	        	mAlarmManager.cancel(mAlarmPIntent);
	        	finishAlarmMode();
	        }
	        else if (ACTION_TEST_STARTED.equals(action)) {
	        	mIsTestMode = true;
	        }
	        else if (ACTION_TEST_FINISHED.equals(action)) {
	        	mIsTestMode = false;
	        }
	        else if (ACTION_FINISH_BROADCASTED.equals(action)) {
	        	finishAlarmMode();
	        }
	        else if (ACTION_SYNC_BROADCASTED.equals(action)) {
	        	startSync();
	        }
	        else if (ACTION_BATTERY_BROADCASTED.equals(action)) {
	        	if (((android.bluetooth.BluetoothGatt)mBluetoothGattObj) != null) {
	    			switchNotificationDeviceLe(true);
	    			
	    			if (Build.VERSION.SDK_INT < 19) {
		    			mAlarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + APP_BATTERY_DURATION, mBatteryPIntent);
		    		}
		    		else {
		    			mAlarmManager.setWindow(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + APP_BATTERY_DURATION, 300000, mBatteryPIntent);
		    		}
	    		}
	        }
		}
	};
	
	// Приёмник события выключения телефона
	private final BroadcastReceiver mShutdownReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (Intent.ACTION_SHUTDOWN.equals(action)) {				
				mIsSwitchingOff = true;
			}
	    }
	};
	
	// Слушатель смены местоположения
	private final LocationListener mLocationListener = new LocationListener() {
	    @SuppressLint("SimpleDateFormat")
		@Override
		public void onLocationChanged(Location location) {
			if (MyDevice.hasInternetConnection(getApplicationContext())) {
	    		HashMap<String, String> params = new HashMap<String, String>();
	    				
	    		params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
	    		params.put("imei", MyDevice.getImei(getApplicationContext()));
	    		params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
	    		params.put("alarm_id", Integer.toString(mAlarmId));
	    		params.put("datetime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	    		params.put("lat_auto", Double.toString(location.getLatitude()));
	    		params.put("long_auto", Double.toString(location.getLongitude()));
	    		params.put("lat_user", "0");
	    		params.put("long_user", "0");
	    					
	    		mGpsHttpTask = new GpsHttpTask();
				mGpsHttpTask.execute("alarm_gps", params, null);
	    	}
	    	else {
	    		showNotification("Отсутствует соединение с интернет!");
	    	}
		}

		@Override
		public void onProviderDisabled(String provider) {}

		@Override
		public void onProviderEnabled(String provider) {}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	};
	
	// Обработчик потоковых сообщений
	static class MyHandler extends Handler {
		WeakReference<AlarmService> mRef;

		private MyHandler(AlarmService service) {
			mRef = new WeakReference<AlarmService>(service);
		}
		
		public void handleMessage(Message msg) {
	        AlarmService context = mRef.get();
	        if (context == null) return;
	        
	        switch (msg.what) {
	            case BLUETOOTH_RESPONSE:
	            	if (context.mAlarmId != 0) {
	            		break;
	            	}
	            	
	            	String buf = "";
	            	StringBuilder sb = new StringBuilder();
	            	
	            	byte[] readBuf = (byte[])msg.obj;
	            	
	            	sb.append(new String(readBuf, 0, msg.arg1));
	            	int eofIndex = sb.indexOf("\n\r");
	                
	            	if (eofIndex > 0) {
	            		buf = sb.substring(0, eofIndex);
	            	}
	            	else {
	            		buf = sb.toString();
	            	}
	            	
	            	context.mButtonId = context.getButtonPressed(buf);
	            	
	            	if (context.mButtonId > 0) {
	            		if (!context.mIsTestMode) {
	            			if (!context.mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {
	            				context.doAction();
	            			}
	            		}
	            		else {
	            			Intent intent = new Intent(ACTION_TEST_SENT);
	            			intent.putExtra("button", context.mButtonId);
	            			context.sendBroadcast(intent);
	            		}
	            	}
	            	break;
	            	
	            case SHOW_TOAST:
	            	Toast.makeText(context, (String)msg.obj, Toast.LENGTH_SHORT).show();
	            	break;
	        }
		}
	}
	   
	// Обработка HTTP-запроса отправки тревоги
	private class AlarmHttpTask extends AsyncTask<Object, Void, MyHttpClient> {
		@SuppressWarnings("unchecked")
		@Override
		protected MyHttpClient doInBackground(Object... params) {
			MyHttpClient http = new MyHttpClient();
			http.doPost((String)params[0], (HashMap<String, String>)params[1], (HashMap<String, byte[]>)params[2]);
			
			return http;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(MyHttpClient http) {
			super.onPostExecute(http);
			
	    	if (http.isSuccess()) {
	    		if (http.getInt("status") > 0) {
	    			mAlarmId = http.getInt("alarm_id");
	    			
	    			if (Build.VERSION.SDK_INT < 19) {
	    				mAlarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + ALARM_MODE_DURATION, mAlarmPIntent);
	    			}
	    			else {
	    				mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + ALARM_MODE_DURATION, mAlarmPIntent);
	    			}
	    			
	    			doConfirmation();
	    			startAlarmMode();
	    		}
	    		else {
	    			mAlarmId = 0;
	    			mButtonId = 0;
	    			
	    			showNotification(http.getString("error"));
	    		}
	    	}
	    	else {
	    		mAlarmId = 0;
	    		mButtonId = 0;
	    		
	    		showNotification("Невозможно соединиться с сервером!");
	    	}
		}
	}
		
	// Обработка HTTP-запроса отправки GPS-координат
	private class GpsHttpTask extends AsyncTask<Object, Void, MyHttpClient> {
		@SuppressWarnings("unchecked")
		@Override
		protected MyHttpClient doInBackground(Object... params) {
			MyHttpClient http = new MyHttpClient();
			http.doPost((String)params[0], (HashMap<String, String>)params[1], (HashMap<String, byte[]>)params[2]);
			
			return http;
		}

		@Override
		protected void onPostExecute(MyHttpClient http) {
			super.onPostExecute(http);
			
	    	if (http.isSuccess()) {
	    		if (http.getInt("status") == 0) {
	    			showNotification(http.getString("error"));
	    		}
	    	}
	    	else {
	    		showNotification("Невозможно соединиться с сервером!");
	    	}
		}
	}
	
	// Обработка HTTP-запроса синхронизации
	private class SyncHttpTask extends AsyncTask<Object, Void, MyHttpClient> {
		@SuppressWarnings("unchecked")
		@Override
		protected MyHttpClient doInBackground(Object... params) {
			MyHttpClient http = new MyHttpClient();
			http.doPost((String)params[0], (HashMap<String, String>)params[1], (HashMap<String, byte[]>)params[2]);
			
			return http;
		}

		@Override
		protected void onPostExecute(MyHttpClient http) {
			super.onPostExecute(http);
			
	    	if (http.isSuccess()) {
	    		if (http.getInt("status") > 0) {
	    			Editor editor = mSettings.edit();
					editor.putInt(MyDevice.APP_PREFS_MODE_CONFS, http.getInt("mode_confs"));
					editor.putString(MyDevice.APP_PREFS_LABEL_1, http.getString("label", new String[] {"buttons", "button"}));
					editor.putInt(MyDevice.APP_PREFS_CONFIRMATION_1, http.getInt("confirmation", new String[] {"buttons", "button"}));
					editor.putString(MyDevice.APP_PREFS_LABEL_2, http.getString("label", new String[] {"buttons", "button_1"}));
					editor.putInt(MyDevice.APP_PREFS_CONFIRMATION_2, http.getInt("confirmation", new String[] {"buttons", "button_1"}));
					editor.apply();
	    		}
	    		else  {
	    			showNotification(http.getString("error"));
	    		}
	    	}
	    	else {
	    		showNotification("Невозможно соединиться с сервером!");
	    	}
		}
	}		
		
	// Подключение к Bluetooth-устройству
	private class ConnectBluetoothThread extends Thread {
		private final BluetoothSocket mmSocket;

		public ConnectBluetoothThread(BluetoothDevice device) {
			BluetoothSocket tmp = null;

			try {
				tmp = device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
			}
			catch (Exception e) {}
			
			mmSocket = tmp;
		}

		public void run() {
			mBluetoothAdapter.cancelDiscovery();
			
			try {
				mmSocket.connect();

				if (mNoSilent) {
					showToast("Соединение с Bluetooth-устройством установлено!");
				}

				if (mKeySets) {
					startKeySetsActivity();
				}
				
				sendBroadcast(new Intent(ACTION_CONNECT_OK_RESPONSED));
				startSync();

				mIOBluetoothThread = new IOBluetoothThread(mmSocket);
				mIOBluetoothThread.start();
			}
			catch (Exception e) {
				if (!mIsSwitchingOff) {
					sendBroadcast(new Intent(ACTION_CONNECT_FAIL_RESPONSED));
					
					if (mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {
						if (mNoSilent) {
							showToast("Невозможно подключиться к Bluetooth-устройству!");
						}
					}
					else {
						startReconnectActivity();
					}			
					
					stopSelf();
				}
			}
		}

		public void cancel() {
			try {
				mIsSwitchingOff = true;
				mmSocket.close();
			}
			catch (Exception e) {}
		}
	}

	// Приём/передача данных Bluetooth
	private class IOBluetoothThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;

		public IOBluetoothThread(BluetoothSocket socket) {
			mmSocket = socket;
			InputStream tmpIn = null;

			try {
				tmpIn = mmSocket.getInputStream();
			}
			catch (Exception e) {}

			mmInStream = tmpIn;
		}

		public void run() {
			byte[] buffer = new byte[1024];
			int bytes;

			while (true) {
				try {
					bytes = mmInStream.read(buffer);                    
					mHandler.obtainMessage(BLUETOOTH_RESPONSE, bytes, -1, buffer).sendToTarget();
				}
				catch (Exception e) {
					if (!mIsSwitchingOff) {
						sendBroadcast(new Intent(ACTION_CONNECT_FAIL_RESPONSED));
						
						if (mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {
							if (mNoSilent) {
								showToast("Связь с Bluetooth-устройством потеряна!");
								startDiscoverActivity();
							}
						}
						else {
							startReconnectActivity();
						}
						
						stopSelf();
					}
					break;
				}
			}
		}

		public void cancel() {
			try {
				mIsSwitchingOff = true;
				mmSocket.close();
			}
			catch (Exception e) {}
		}
	}	
    
    
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		// Инициализируем настройки
		mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
		
		// Регистрируем приёмник событий Bluetooth
        registerReceiver(mBluetoothStateReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        
        // Регистрируем приёмник событий AlarmService
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_ALARM_REQUESTED);
        filter.addAction(ACTION_ALARM_SENT);
        filter.addAction(ACTION_ALARM_CANCELED);
        filter.addAction(ACTION_TEST_STARTED);
        filter.addAction(ACTION_TEST_FINISHED);
        filter.addAction(ACTION_FINISH_BROADCASTED);
        filter.addAction(ACTION_SYNC_BROADCASTED);
        filter.addAction(ACTION_BATTERY_BROADCASTED);
        registerReceiver(mAlarmReceiver, filter);
        
        // Регистрируем приёмник события выключения телефона
        registerReceiver(mShutdownReceiver, new IntentFilter(Intent.ACTION_SHUTDOWN));
        
        // Подключаем обработчик потоков
        mHandler = new MyHandler(this);
        
        // Подключаем системный таймер
     	mAlarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
     	mAlarmPIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_FINISH_BROADCASTED), 0);
     	mSyncPIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_SYNC_BROADCASTED), 0);
     	mBatteryPIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_BATTERY_BROADCASTED), 0);
        
        // Подключаем службу местоположения
        mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        
        // Подключаем уведомления
        mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        
        // Подключаем вибратор
        mVibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Включаем постоянную работу
		startForeground(1, getMainNotification());
		
		// Запускать ли с уведомлениями
		if (intent != null) {
			mNoSilent = intent.getBooleanExtra("no_silent", false);
			mKeySets = intent.getBooleanExtra("key_sets", false);
		}
		
		// Запуск службы
		if (!mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00").equals("00:00:00:00:00:00")) {
			testBluetooth();
		}
		else {
			if (mSettings.getBoolean(MyDevice.APP_PREFS_NO_DEVICE, false)) {
				if (mNoSilent) {
                	showToast("Служба АйКнопка запущена успешно!");
                }
                
                if (mKeySets) {
                	startKeySetsActivity();
                }
                
                startSync();
			}
			else {
				stopSelf();
			}
		}
		return START_STICKY;
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем постоянную работу
		stopForeground(true);
		
		// Отключаем приёмники
		unregisterReceiver(mBluetoothStateReceiver);
		unregisterReceiver(mAlarmReceiver);
		unregisterReceiver(mShutdownReceiver);
		
		// Отключаем поиск местоположения
		mLocationManager.removeUpdates(mLocationListener);
		
		// Останавливаем потоки Bluetooth
		if (mConnectBluetoothThread != null) {
			mConnectBluetoothThread.cancel();
		}
		
		if (mIOBluetoothThread != null) {
			mIOBluetoothThread.cancel();
		}
		
		if (MyDevice.hasBluetoothLe(this)) {
			connectLeDevice(false);
		}
		
		// Останавливаем отложенные действия
		mAlarmManager.cancel(mAlarmPIntent);
		mAlarmManager.cancel(mSyncPIntent);
		mAlarmManager.cancel(mBatteryPIntent);
		
		// Удаляем уведомления
		mNotificationManager.cancel(2);
		
		// Освобождаем звук
		releaseSound();
	}
	
	
	
	// Проверка Bluetooth
    private void testBluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        
        if (mBluetoothAdapter == null) {
        	showToast("Bluetooth не поддерживается на вашем устройстве!\nСлужба АйКнопка будет остановлена.");
        	
        	Editor editor = mSettings.edit();
			editor.putString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00");
			editor.putBoolean(MyDevice.APP_PREFS_NO_DEVICE, false);
			editor.apply();
        	
			sendBroadcast(new Intent(ACTION_CONNECT_FAIL_RESPONSED));
        	stopSelf();
        	
        	return;
        }
        
        if (!mBluetoothAdapter.isEnabled()) {
        	mBluetoothAdapter.enable();
        }
        else {
        	connectBluetoothDevice();
        }
    }
    
    // Подключение к устройству Bluetooth
    @SuppressLint("NewApi")
	private void connectBluetoothDevice() {
    	BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
    	    	
    	if (mNoSilent) {
    		showToast("Выполняется соединение с Bluetooth-устройством...");
    	}
    	
    	int deviceType = 0;
    	
    	if (Build.VERSION.SDK_INT >= 18) {
        	deviceType = device.getType();
    	}
		
		if (deviceType == 0 || deviceType == 1) {
			mConnectBluetoothThread = new ConnectBluetoothThread(device);
	    	mConnectBluetoothThread.start();
		}
		else if (deviceType == 2) {
			connectLeDevice(true);
		}
		else if (deviceType == 3) {
			if (MyDevice.hasBluetoothLe(this)) {
				connectLeDevice(true);
			}
			else {
				mConnectBluetoothThread = new ConnectBluetoothThread(device);
		    	mConnectBluetoothThread.start();
			}
		}
    }
    
    // Подключение к устройству Bluetooth Low Energy
    @SuppressLint("NewApi")
	private void connectLeDevice(boolean enable) {
		android.bluetooth.BluetoothGattCallback gattCallback = new android.bluetooth.BluetoothGattCallback() {
			@Override
			public void onConnectionStateChange(android.bluetooth.BluetoothGatt gatt, int status, int newState) {
				if (newState == android.bluetooth.BluetoothProfile.STATE_CONNECTED) {
					if (mNoSilent) {
						showToast("Соединение с Bluetooth-устройством установлено!");
					}

					if (mKeySets) {
						startKeySetsActivity();
					}
					
					sendBroadcast(new Intent(ACTION_CONNECT_OK_RESPONSED));
					startSync();
					
					gatt.discoverServices();
				}
				else if (newState == android.bluetooth.BluetoothProfile.STATE_DISCONNECTED) {
					if (!mIsSwitchingOff) {
						sendBroadcast(new Intent(ACTION_CONNECT_FAIL_RESPONSED));
						
						if (mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {
							if (mNoSilent) {
								showToast("Соединение с Bluetooth-устройством разорвано!");
							}
						}
						else {
							startReconnectActivity();
						}			
						
						stopSelf();
					}
				}
			}
			
			@Override
			public void onServicesDiscovered(android.bluetooth.BluetoothGatt gatt, int status) {
				if (status == android.bluetooth.BluetoothGatt.GATT_SUCCESS) {
					switchNotificationDeviceLe(true);
					
					if (Build.VERSION.SDK_INT < 19) {
		    			mAlarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + APP_BATTERY_DURATION, mBatteryPIntent);
		    		}
		    		else {
		    			mAlarmManager.setWindow(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + APP_BATTERY_DURATION, 300000, mBatteryPIntent);
		    		}
				}
			}
			
			@Override
			public void onCharacteristicRead(android.bluetooth.BluetoothGatt gatt, android.bluetooth.BluetoothGattCharacteristic characteristic, int status) {
				if (status == android.bluetooth.BluetoothGatt.GATT_SUCCESS) {
					String buf = "";
					byte[] data = characteristic.getValue();
	            	
	            	if (data != null && data.length > 0) {
	            	StringBuilder sb = new StringBuilder(data.length);
	                	
	                	for (byte byteChar : data) {
	                		sb.append(String.format("%02X", byteChar));
	                	}
	                
	                	buf = sb.toString();
	            	}
	            	
	            	if (!mLastBatteryChargeHex.equals(buf)) {
	            		if (buf.equals("0F")) {
	            			showNotification("Заряд батареи брелка ниже 15%");
	            		}
	            		else if (buf.equals("0A")) {
	            			showNotification("Заряд батареи брелка ниже 10%");
	            		}
	            		else if (buf.equals("05")) {
	            			showNotification("Заряд батареи брелка ниже 5%");
	            		}
	            		else if (buf.equals("02")) {
	            			showNotification("Заряд батареи брелка ниже 2%");
	            		}
	            		else if (buf.equals("01")) {
	            			showNotification("Батарея брелка разряжена!");
	            		}
	            		
	            		mLastBatteryChargeHex = buf;
	            	}
	            	
	            	switchNotificationDeviceLe(false);
				}
			}
			
			@Override
			public void onCharacteristicChanged(android.bluetooth.BluetoothGatt gatt, android.bluetooth.BluetoothGattCharacteristic characteristic) {
				if (mAlarmId != 0) {
            		return;
            	}
            	
            	String buf = "";
            	mButtonId = 0;
            	
            	byte[] data = characteristic.getValue();
            	
                if (data != null && data.length > 0) {
                	StringBuilder sb = new StringBuilder(data.length);
                	
                	for (byte byteChar : data) {
                		sb.append(String.format("%02X", byteChar));
                	}

                	buf = sb.toString();
                }
            	
            	if (buf.equals("AF")) {
            		mButtonId = 1;
                }
                else {
                	mButtonId = 2;
                }
            	
            	if (mButtonId > 0) {
            		if (!mIsTestMode) {
            			if (!mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {
            				doAction();
            			}
            		}
            		else {
            			Intent intent = new Intent(ACTION_TEST_SENT);
            			intent.putExtra("button", mButtonId);
            			sendBroadcast(intent);
            		}
            	}
			}
			
			@Override
			public void onDescriptorWrite(android.bluetooth.BluetoothGatt gatt, android.bluetooth.BluetoothGattDescriptor descriptor, int status) {
				if (status == android.bluetooth.BluetoothGatt.GATT_SUCCESS) {
					if (descriptor.getValue() == android.bluetooth.BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE) {
						android.bluetooth.BluetoothGattService gattBatteryService = ((android.bluetooth.BluetoothGatt)mBluetoothGattObj).getService(UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb"));
						android.bluetooth.BluetoothGattCharacteristic gattBatteryCharacteristic = gattBatteryService.getCharacteristic(UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb"));
						((android.bluetooth.BluetoothGatt)mBluetoothGattObj).readCharacteristic(gattBatteryCharacteristic);
					}
				}
			}
		};
		
		if (enable) {
			BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
			mBluetoothGattObj = device.connectGatt(this, false, gattCallback);
    	}
    	else {
    		if (mBluetoothGattObj == null) {
        		return;
        	}
        	
        	mIsSwitchingOff = true;
        	
        	((android.bluetooth.BluetoothGatt)mBluetoothGattObj).close();
        	mBluetoothGattObj = null;
        }
    }
    
    // Управление оповещениями устройства Bluetooth Low Energy
    @SuppressLint("NewApi")
	private void switchNotificationDeviceLe(boolean enable) {
    	if (mBluetoothGattObj != null) {    	
    		android.bluetooth.BluetoothGattService gattService = ((android.bluetooth.BluetoothGatt)mBluetoothGattObj).getService(UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb"));
    		android.bluetooth.BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb"));
    		
    		((android.bluetooth.BluetoothGatt)mBluetoothGattObj).setCharacteristicNotification(gattCharacteristic, true);
    		
    		android.bluetooth.BluetoothGattDescriptor gattDescriptor = gattCharacteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
    		gattDescriptor.setValue(enable ? android.bluetooth.BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE : android.bluetooth.BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
    		((android.bluetooth.BluetoothGatt)mBluetoothGattObj).writeDescriptor(gattDescriptor);
    	}
    }
    
    // Получение нажатой кнопки Bluetooth
    private int getButtonPressed(String str) {
    	int resp = 0;
    	
    	try {
    		int data = Integer.parseInt(str, 16);    	
    		str = Integer.toBinaryString(data);
    		
    		while (str.length() < 8) {
    			str = "0" + str;
    		}
    		
    		if (str.charAt(7) == '1' && str.charAt(6) == '0') {
    			resp = 1;
    	    }
    	    else if (str.charAt(7) == '0' && str.charAt(6) == '1') {
    	    	resp = 1;
    	    }
    	    else if (str.charAt(7) == '1' && str.charAt(6) == '1') {
    	    	resp = 2;
    	    }
    	} catch (Exception e) {}
    	
    	return resp;
    }
    
    // Выполнение действия
    @SuppressLint("SimpleDateFormat")
	private void doAction() {
    	int action = 0;
    	
    	switch (mButtonId) {
    		case 1:
    			action = 0;
    			break;
    		case 2:
    			action = 1;
    			break;
    	}
    	
    	if (MyDevice.hasInternetConnection(this)) {
    		HashMap<String, String> params = new HashMap<String, String>();
    				
    		params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
    		params.put("imei", MyDevice.getImei(this));
    		params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
    		params.put("datetime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    		params.put("action", Integer.toString(action));
    		params.put("test", "0");
    		
    		mAlarmId = -1;
    		
    		mAlarmHttpTask = new AlarmHttpTask();
			mAlarmHttpTask.execute("alarm_send", params, null);
    	}
    	else {
    		showNotification("Отсутствует соединение с интернет!");
    	}
    }
    
    // Выполнение подтверждения
    private void doConfirmation() {
    	String confirmation = MyDevice.APP_PREFS_CONFIRMATION_1;
    	
    	switch (mButtonId) {
			case 1:
				confirmation = MyDevice.APP_PREFS_CONFIRMATION_1;
				break;
			case 2:
				confirmation = MyDevice.APP_PREFS_CONFIRMATION_2;
				break;
    	}
    	
    	switch (mSettings.getInt(confirmation, 0)) {
    		case 0:
    			break;
    		case 1:
    			if (mVibrator != null) {
    				mVibrator.vibrate(new long[] {0, 100, 100, 1000}, -1);
    			}
    			break;
    		case 2:
    			if (mVibrator != null) {
    				mVibrator.vibrate(new long[] {0, 100, 100, 1000, 1000, 100, 100, 1000}, -1);
    			}
    			break;
    		case 3:
    			if (mVibrator != null) {
    				mVibrator.vibrate(new long[] {0, 2000, 100, 2000, 100, 2000}, -1);
    			}
    			break;
    		case 4:
    			playSound(R.raw.sound1);
    			break;
    		case 5:
    			playSound(R.raw.sound2);
    			break;
    		case 6:
    			playSound(R.raw.sound3);
    			break;
    	}
    }
    
    // Действия начала тревоги
    private void startAlarmMode() {
    	boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    	boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    	
    	if (isNetworkEnabled) {
    		mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_DURATION, 0, mLocationListener);
    	}
    	
    	if (isGPSEnabled) {
    		mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_DURATION, 0, mLocationListener);
    	}
    	
    	Editor editor = mSettings.edit();
		editor.putInt(MyDevice.APP_PREFS_ATTEMPTS, 10);
		editor.apply();
    	
    	if (mSettings.getInt(MyDevice.APP_PREFS_MODE_CONFS, 0) == 0) {
    		startMainActivity();
    	}
    	
    	Intent intent = new Intent(ACTION_ALARM_STARTED);
    	intent.putExtra("alarm_id", mAlarmId);
    	intent.putExtra("button_id", mButtonId);
    	sendBroadcast(intent);
    }
    
    // Действия окончания тревоги
    private void finishAlarmMode() {
    	mLocationManager.removeUpdates(mLocationListener);
    	
    	mNotificationManager.notify(1, getMainNotification());
    	
    	mAlarmId = 0;
    	mButtonId = 0;
    	
		sendBroadcast(new Intent(ACTION_ALARM_FINISHED));
    }
    
    // Вывод уведомления из потока
    private void showToast(String str) {
    	mHandler.obtainMessage(SHOW_TOAST, -1, -1, str).sendToTarget();
    }
    
    // Генерация начального уведомления
    @SuppressLint("InlinedApi")
	private Notification getMainNotification() {
    	Intent intent = new Intent(this, SplashActivity.class);
    	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);
    	
    	NotificationCompat.Builder notif = new NotificationCompat.Builder(this)
        	.setContentTitle("АйКнопка")
        	.setContentText("Защита включена")
        	.setWhen(System.currentTimeMillis())
        	.setContentIntent(pIntent)
        	.setOngoing(true)
        	.setSmallIcon(R.drawable.ic_launcher);
        
        return notif.build();
    }
    
    // Вывод любого уведомления в трей
    @SuppressLint("InlinedApi")
	private void showNotification(String str) {
    	Intent intent = new Intent(this, SplashActivity.class);
    	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);
    	
    	NotificationCompat.Builder notif = new NotificationCompat.Builder(this)
        	.setContentTitle("АйКнопка")
        	.setContentText(str)
        	.setTicker(str)
        	.setWhen(System.currentTimeMillis())
        	.setContentIntent(pIntent)
        	.setDefaults(Notification.DEFAULT_ALL)
        	.setAutoCancel(true)
        	.setSmallIcon(R.drawable.ic_launcher);
    	
    	if (Build.VERSION.SDK_INT >= 16) {
			notif.setPriority(Notification.PRIORITY_MAX);
			notif.setVibrate(new long[0]);
		}
        
        mNotificationManager.notify(2, notif.build());
    }
    
    // Запуск окна переподключения приложения
    @SuppressLint("InlinedApi")
	private void startReconnectActivity() {
    	sendBroadcast(new Intent(ACTION_WIZARD_TERMINATED));
        sendBroadcast(new Intent(ACTION_APP_TERMINATED));
    	
        Intent intent = new Intent(this, ReconnectActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);
    	
    	NotificationCompat.Builder notif = new NotificationCompat.Builder(this)
    		.setContentTitle("АйКнопка")
    		.setContentText("Коснитесь для переподключения устройства")
    		.setTicker("Связь с устройством потеряна!")
    		.setWhen(System.currentTimeMillis())
    		.setContentIntent(pIntent)
    		.setDefaults(Notification.DEFAULT_ALL)
    		.setAutoCancel(true)
    		.setSmallIcon(R.drawable.ic_action_warning);
    	
    	if (Build.VERSION.SDK_INT >= 16) {
			notif.setPriority(Notification.PRIORITY_MAX);
			notif.setVibrate(new long[0]);
		}
    	
        mNotificationManager.notify(3, notif.build());
    }
    
    // Запуск окна поиска устройств
    private void startDiscoverActivity() {
    	sendBroadcast(new Intent(ACTION_WIZARD_TERMINATED));
        sendBroadcast(new Intent(ACTION_APP_TERMINATED));
    	
        Intent intent = new Intent(this, DiscoverActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("is_reconnect", true);
    	startActivity(intent);
    }
    
    // Запуск главного окна приложения
    @SuppressLint("InlinedApi")
	private void startMainActivity() {
    	Intent intent = new Intent(this, MainActivity.class);
    	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);
        
        startActivity(intent);
    	
    	NotificationCompat.Builder notif = new NotificationCompat.Builder(this)
        	.setContentTitle("АйКнопка")
        	.setContentText("Нажата тревожная кнопка!")
        	.setTicker("Нажата тревожная кнопка!")
        	.setWhen(System.currentTimeMillis())
        	.setContentIntent(pIntent)
        	.setOngoing(true)
        	.setSmallIcon(R.drawable.ic_launcher);
    	
    	if (Build.VERSION.SDK_INT >= 16) {
			notif.setPriority(Notification.PRIORITY_MAX);
			notif.setVibrate(new long[0]);
		}
        
        mNotificationManager.notify(1, notif.build());
    }
    
    // Запуск окна настройки кнопок
    private void startKeySetsActivity() {
    	Intent intent = new Intent(this, KeySetsActivity.class);
    	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	startActivity(intent);
    }
    
    // Запуск синхронизации
    @SuppressLint("NewApi")
	private void startSync() {
		if (MyDevice.hasInternetConnection(this)) {
    		HashMap<String, String> params = new HashMap<String, String>();
    				
    		params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
    		params.put("imei", MyDevice.getImei(this));
    		params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
    		
    		mSyncHttpTask = new SyncHttpTask();
			mSyncHttpTask.execute("user_modes_get", params, null);
    	}
		
		if (Build.VERSION.SDK_INT < 19) {
			mAlarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + APP_SYNC_DURATION, mSyncPIntent);
		}
		else {
			mAlarmManager.setWindow(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + APP_SYNC_DURATION, 300000, mSyncPIntent);
		}
    }
    
    // Проигрывание звука
    private void playSound(int res) {
    	releaseSound();
    	
    	mMediaPlayer = MediaPlayer.create(this, res);    	
    	if (mMediaPlayer != null) {
    		mMediaPlayer.start();
    	}
    }
    
    // Освобождение звуков
    private void releaseSound() {
    	if (mMediaPlayer != null) {
    		mMediaPlayer.release();
    		mMediaPlayer = null;
    	}
    }
}