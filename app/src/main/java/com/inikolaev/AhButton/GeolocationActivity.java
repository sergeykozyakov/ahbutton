package com.inikolaev.AhButton;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class GeolocationActivity extends FragmentActivity implements AsyncTaskFragment.AsyncTaskCallbacks, OnMapReadyCallback {

	private SharedPreferences mSettings;
	
	private ProgressDialog mProgressDialog;
	private ProgressDialog mStartDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private Button mButtonGeolocationNext;
	private Spinner mSpinnerMapTypes;
	private LocationManager mLocationManager;
    private SupportMapFragment mMapFragment;
	private GoogleMap mMap;
	
	private int mAlarmId = 0;
	
	private double mLatAuto = 0;
	private double mLongAuto = 0;
	private double mLatUser = 0;
	private double mLongUser = 0;
	
	// Приёмник событий тревоги
	private final BroadcastReceiver mAlarmReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_ALARM_FINISHED.equals(action)) {
				finish();
			}
		}
	};
	
	// Приёмник события закрытия окна
	private final BroadcastReceiver mStopReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_APP_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
    
    // Слушатель местоположения по сети
    private final LocationListener mNetworkLocationListener = new LocationListener() {
    	@Override
    	public void onLocationChanged(Location location) {
    		locationReady(location);
    		mLocationManager.removeUpdates(this);
    	}

    	@Override
    	public void onProviderDisabled(String provider) {}

    	@Override
    	public void onProviderEnabled(String provider) {}

    	@Override
    	public void onStatusChanged(String provider, int status, Bundle extras) {}
    };
    
    // Слушатель местоположения по GPS
    private final LocationListener mGPSLocationListener = new LocationListener() {
    	@Override
    	public void onLocationChanged(Location location) {
    		locationReady(location);
    		
    		mLocationManager.removeUpdates(this);
    		mLocationManager.removeUpdates(mNetworkLocationListener);
    	}

    	@Override
    	public void onProviderDisabled(String provider) {}

    	@Override
    	public void onProviderEnabled(String provider) {}

    	@Override
    	public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

	@Override
	public void onMapReady(GoogleMap map) {
        if (map != null) {
            mMap = map;
            mMap.setOnMapClickListener(mMapClickListener);
        }
        else {
            finish();
            Toast.makeText(this, "Невозможно отобразить карту на вашем устройстве!", Toast.LENGTH_SHORT).show();
            return;
        }

        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (MyDevice.isAirplaneModeOn(this) && !isGPSEnabled) {
            finish();
            Toast.makeText(this, "Для определения местоположения необходимо подключиться к сотовой сети или включить GPS!", Toast.LENGTH_SHORT).show();
        }
        else {
            if (!isGPSEnabled && !isNetworkEnabled) {
                finish();
                Toast.makeText(this, "Нет данных! Проверьте настройки поиска местоположения в меню телефона.", Toast.LENGTH_SHORT).show();
            }
            else {
                if (isNetworkEnabled) {
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mNetworkLocationListener);
                }

                if (isGPSEnabled) {
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mGPSLocationListener);
                }
            }
        }
	}

    // Действия при нахождении местоположения
    private void locationReady(Location location) {
    	mStartDialog.dismiss();

        if (mMap == null) {
            return;
        }

    	mMap.clear();
		mMap.addMarker(new MarkerOptions()
			.position(new LatLng(location.getLatitude(), location.getLongitude()))
			.title("Я здесь"));
		
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 16));
		
		mLatAuto = location.getLatitude();
		mLongAuto = location.getLongitude();
		
		mLatUser = mLatAuto;
		mLongUser = mLongAuto;
    }
	
    // Слушатель нажатия на карту
    private final OnMapClickListener mMapClickListener = new OnMapClickListener() {
		@Override
		public void onMapClick(LatLng point) {
			mLocationManager.removeUpdates(mNetworkLocationListener);
			mLocationManager.removeUpdates(mGPSLocationListener);
			
			mMap.clear();
    		mMap.addMarker(new MarkerOptions()
    				.position(point)
    				.title("Я здесь"));
    		
    		mLatUser = point.latitude;
    		mLongUser = point.longitude;
		}
    };
    
	// Слушатель нажатия на кнопку подтверждения местоположения
	@SuppressLint("SimpleDateFormat")
	private final OnClickListener mButtonGeolocationNextClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
	    	if (MyDevice.hasInternetConnection(getApplicationContext())) {
	    		HashMap<String, String> params = new HashMap<String, String>();
	    				
	    		params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
	    		params.put("imei", MyDevice.getImei(getApplicationContext()));
	    		params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
	    		params.put("alarm_id", Integer.toString(mAlarmId));
	    		params.put("datetime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	    		params.put("lat_auto", Double.toString(mLatAuto));
	    		params.put("long_auto", Double.toString(mLongAuto));
	    		params.put("lat_user", Double.toString(mLatUser));
	    		params.put("long_user", Double.toString(mLongUser));
	    		
				mAsyncTaskFragment.execute("alarm_gps", params, null);
	    	}
	    	else {
	    		Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
	    	}
		}
	};
	
	// Слушатель выбора вида карты
	private final OnItemSelectedListener mSpinnerMapTypesSelect = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
			if (mMap == null) {
				return;
			}
			
			if (position == 0) {
				mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			}
			else {
				mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {}	
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				mProgressDialog.dismiss();
				Toast.makeText(this, "Местоположение отправлено!", Toast.LENGTH_SHORT).show();
				
				finish();
			}
			else {
				mProgressDialog.dismiss();
				
				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
			}
		}
		else {
			mProgressDialog.dismiss();
			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_geolocation);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
	    
        // Регистрируем приёмник событий AlarmService
        registerReceiver(mAlarmReceiver, new IntentFilter(AlarmService.ACTION_ALARM_FINISHED));
        
	    // Регистрируем приёмник события закрытия окна
	    registerReceiver(mStopReceiver, new IntentFilter(AlarmService.ACTION_APP_TERMINATED));
        
        // Кнопка "Подтвердить местоположение"
        mButtonGeolocationNext = (Button)findViewById(R.id.buttonGeolocationNext);
        mButtonGeolocationNext.setOnClickListener(mButtonGeolocationNextClick);
        
        // Выбор вида карты
        mSpinnerMapTypes = (Spinner)findViewById(R.id.spinnerMapTypes);
        mSpinnerMapTypes.setOnItemSelectedListener(mSpinnerMapTypesSelect);
        
        // Получаем данные
        mAlarmId = getIntent().getIntExtra("alarm_id", 0);
        
        // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    // Показываем окно загрузки
	    mStartDialog = new ProgressDialog(this);
	    mStartDialog.setMessage("Поиск местоположения");
		mStartDialog.setCancelable(false);
		mStartDialog.show();

        // Подключаем службу местоположения
        mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		
	    // Подключаем карту
        mMapFragment = (SupportMapFragment)fm.findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмники
		unregisterReceiver(mAlarmReceiver);
		unregisterReceiver(mStopReceiver);
		
		// Отключаем поиск местоположения
		if (mLocationManager != null) {
			mLocationManager.removeUpdates(mNetworkLocationListener);
			mLocationManager.removeUpdates(mGPSLocationListener);
		}
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}
