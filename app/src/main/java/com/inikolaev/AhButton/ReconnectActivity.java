package com.inikolaev.AhButton;

import java.util.HashMap;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ReconnectActivity extends FragmentListActivity implements AsyncTaskFragment.AsyncTaskCallbacks {
	
	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AlertDialog mExitDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private String mReason = "0";
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				Editor editor = mSettings.edit();
				editor.putBoolean(MyDevice.APP_PREFS_LIC, false);
				editor.putBoolean(MyDevice.APP_PREFS_REGISTERING, false);
				editor.putString(MyDevice.APP_PREFS_LOGIN, "");
				editor.putString(MyDevice.APP_PREFS_PASSWORD, "");
				editor.putString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00");
				editor.putBoolean(MyDevice.APP_PREFS_NO_DEVICE, false);
				editor.apply();
				
				mProgressDialog.dismiss();
				
				Toast.makeText(this, "Обслуживание остановлено!\nДля продолжения работы запустите приложение АйКнопка снова.", Toast.LENGTH_LONG).show();
				finish();
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
			}
		}
		else {
			mProgressDialog.dismiss();
			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_reconnect);
	    
	    if (savedInstanceState != null) {
	    	mReason = savedInstanceState.getString("reason");
	    	
	    	int dialogShowing = savedInstanceState.getInt("dialog_showing");
	    	
	    	if (dialogShowing > 0) {
	    		showAlertDialog(dialogShowing);
	    	}
	    }
	    
        // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    if (mAsyncTaskFragment.isTaskRunning()) {
	    	showProgressDialog();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
	    
        // Готовим список действий
        String[] items = new String[] { "Брелок потерян", "Брелок отдан/отключен", "Подключить брелок" };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        setListAdapter(adapter);
	}
	
    @Override
	protected int getSupportLayoutResourceId() {
		return R.layout.activity_reconnect;
	}

    @Override
    public void onResume() {
    	super.onResume();
    	
    	// Сброс на экран запуска
    	if (MyDevice.isAlarmServiceRunning(this) || mSettings.getString(MyDevice.APP_PREFS_LOGIN, "").equals("")) {
    		startActivity(new Intent(this, SplashActivity.class));
    		finish();
    	}
    }
	
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
		if (position == 0 || position == 1) {
			// Потерял брелок/Подарил брелок
			switch (position) {
				case 0:
					mReason = "1";
					break;
				case 1:
					mReason = "2";
					break;
			}
			
			showAlertDialog(1);
		}
		else if (position == 2) {
			// Подключить брелок
			Intent intent = new Intent(this, DiscoverActivity.class);
			intent.putExtra("is_reconnect", true);
			startActivity(intent);
			
			finish();
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		
		savedInstanceState.putString("reason", mReason);
		
		int dialogShowing = 0;
		
		if (mExitDialog != null && mExitDialog.isShowing()) {
			dialogShowing = 1;
		}
		
		savedInstanceState.putInt("dialog_showing", dialogShowing);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
	
	// Показ информационного диалога
	private void showAlertDialog(int id) {
		switch (id) {
			case 1:
				mExitDialog = new AlertDialog.Builder(ReconnectActivity.this)
		        .setMessage("Вы подтверждаете данное действие?")
		        .setCancelable(true)
		        .setPositiveButton("Да", new DialogInterface.OnClickListener() {
		        	public void onClick(DialogInterface dialog, int id) {
		                dialog.dismiss();
		                
		    	    	if (MyDevice.hasInternetConnection(getApplicationContext())) {
		    	    		HashMap<String, String> params = new HashMap<String, String>();
		    	    	
		    	    		params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
		    	    		params.put("imei", MyDevice.getImei(getApplicationContext()));
		    	    		params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
		    	    		params.put("reason", mReason);
		    	    		
		    	    		mReason = "0";
		    				mAsyncTaskFragment.execute("user_logout", params, null);
		    	    	}
		    	    	else {
		    	    		Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
		    	    	}
		            }
		        })
		        .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
		        	public void onClick(DialogInterface dialog, int id) {
		        		dialog.dismiss();
		        	}
		        })
		        .create();
				
				mExitDialog.show();
				break;
		}
	}
}
