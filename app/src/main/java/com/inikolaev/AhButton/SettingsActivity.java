package com.inikolaev.AhButton;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class SettingsActivity extends FragmentListActivity implements AsyncTaskFragment.AsyncTaskCallbacks {

	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AlertDialog mExitDialog;
	private AlertDialog mDisconnectDeviceDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private Button mButtonSettingsNext;
	
	private ArrayAdapter<String> mAdapterConnected;
	private ArrayAdapter<String> mAdapterConnectedNoDevice;
	private ArrayAdapter<String> mAdapterDisconnected;
	
	// Приёмник события закрытия мастера
	private final BroadcastReceiver mWizardReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_WIZARD_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Далее"
	private final OnClickListener mButtonSettingsNextClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startKeySetsActivity();
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.getParam("_to").equals("modes")) {
    		if (http.isSuccess()) {
    			if (http.getInt("status") > 0) {
    				Editor editor = mSettings.edit();
					editor.putInt(MyDevice.APP_PREFS_MODE_CONFS, http.getInt("mode_confs"));
					editor.putString(MyDevice.APP_PREFS_LABEL_1, http.getString("label", new String[] {"buttons", "button"}));
					editor.putInt(MyDevice.APP_PREFS_CONFIRMATION_1, http.getInt("confirmation", new String[] {"buttons", "button"}));
					editor.putString(MyDevice.APP_PREFS_LABEL_2, http.getString("label", new String[] {"buttons", "button_1"}));
					editor.putInt(MyDevice.APP_PREFS_CONFIRMATION_2, http.getInt("confirmation", new String[] {"buttons", "button_1"}));
					
					if (!MyDevice.isAlarmServiceRunning(this)) {
						editor.putString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00");
						editor.putBoolean(MyDevice.APP_PREFS_NO_DEVICE, true);
						editor.apply();
						
						mProgressDialog.dismiss();
						
    					// Запуск службы AlarmService
    					Intent intent = new Intent(this, AlarmService.class);
        				intent.putExtra("no_silent", true);
        				intent.putExtra("key_sets", true);
        				startService(intent);
					}
					else {
						editor.apply();
						
    					mProgressDialog.dismiss();
						startActivity(new Intent(this, KeySetsActivity.class));
					}
    			}
    			else {
    				mProgressDialog.dismiss();
    				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
    			}
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
			}
		}
		else if (http.getParam("_to").equals("logout")) {
    		if (http.isSuccess()) {
    			if (http.getInt("status") > 0) {
    				stopService(new Intent(this, AlarmService.class));
    				
    				Editor editor = mSettings.edit();
    				editor.putBoolean(MyDevice.APP_PREFS_LIC, false);
    				editor.putBoolean(MyDevice.APP_PREFS_REGISTERING, false);
    				editor.putString(MyDevice.APP_PREFS_LOGIN, "");
    				editor.putString(MyDevice.APP_PREFS_PASSWORD, "");
    				editor.putString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00");
    				editor.putBoolean(MyDevice.APP_PREFS_NO_DEVICE, false);
    				editor.apply();
    				
    				mProgressDialog.dismiss();
    				
    				Toast.makeText(this, "Обслуживание остановлено!", Toast.LENGTH_SHORT).show();
    				
    				sendBroadcast(new Intent(AlarmService.ACTION_WIZARD_TERMINATED));
    		        sendBroadcast(new Intent(AlarmService.ACTION_APP_TERMINATED));
    			}
    			else {
    				mProgressDialog.dismiss();
    				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
    			}
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_settings);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    if (savedInstanceState != null) {
	    	int dialogShowing = savedInstanceState.getInt("dialog_showing");
	    	
	    	if (dialogShowing > 0) {
	    		showAlertDialog(dialogShowing);
	    	}
	    }
	    
        // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    if (mAsyncTaskFragment.isTaskRunning()) {
	    	showProgressDialog();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        // Регистрируем приёмник события закрытия
        registerReceiver(mWizardReceiver, new IntentFilter(AlarmService.ACTION_WIZARD_TERMINATED));
        
        // Кнопка "Далее"
	    mButtonSettingsNext = (Button)findViewById(R.id.buttonSettingsNext);
	    mButtonSettingsNext.setOnClickListener(mButtonSettingsNextClick);
        
        // Готовим списки действий
        String[] itemsConnected = new String[] { "Купить брелок", "Настройка кнопок", "Проверка оборудования", "Работать без брелка", "Сменить пароль", "Остановить обслуживание" };
        mAdapterConnected = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, itemsConnected);
        
        String[] itemsConnectedNoDevice = new String[] { "Купить брелок", "Подключить брелок", "Настройка кнопок", "Сменить пароль", "Остановить обслуживание" };
        mAdapterConnectedNoDevice = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, itemsConnectedNoDevice);
        
        String[] itemsDisconnected = new String[] { "Купить брелок", "Подключить брелок", "Работать без брелка" };
        mAdapterDisconnected = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, itemsDisconnected);
    }
	
    @Override
	protected int getSupportLayoutResourceId() {
		return R.layout.activity_settings;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		// Скрываем лишние элементы
		if (MyDevice.isAlarmServiceRunning(this)) {
			if (mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {
				mButtonSettingsNext.setVisibility(View.VISIBLE);
			}
			
			if (mSettings.getBoolean(MyDevice.APP_PREFS_NO_DEVICE, false)) {
				setListAdapter(mAdapterConnectedNoDevice);
			}
			else {
				setListAdapter(mAdapterConnected);
			}
	    }
	    else {
	    	setListAdapter(mAdapterDisconnected);
	    }
	}
	
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        int item = 0;
		
        // Выбор режима отображения
        if (getListAdapter() == mAdapterConnected) {
        	switch (position) {
        	case 0:
        		item = 100;
        		break;
        	case 1:
        		item = 200;
        		break;
        	case 2:
        		item = 500;
        		break;
        	case 3:
        		item = 700;	
        		break;
        	case 4:
        		item = 600;
        		break;
        	case 5:
        		item = 400;
        		break;
        	}
        }
        else if (getListAdapter() == mAdapterConnectedNoDevice) {
        	switch (position) {
        	case 0:
        		item = 100;
        		break;
        	case 1:
        		item = 300;
        		break;
        	case 2:
        		item = 200;
        		break;
        	case 3:
        		item = 600;
        		break;
        	case 4:
        		item = 400;
        		break;
        	}
        }
        else {
        	switch (position) {
        	case 0:
        		item = 100;
        		break;
        	case 1:
        		item = 300;
        		break;
        	case 2:
        		item = 200;
        		break;
        	}
        }
        
        // Выполнение действий нажатия
        switch (item) {
        	// Купить брелок
        	case 100:
        		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://ahbutton.ru/")));
        		break;
        	
        	// Работать без брелка/Настройка кнопок
        	case 200:
        		startKeySetsActivity();
    			break;
        	
        	// Подключить брелок
        	case 300:
        		startActivity(new Intent(this, DiscoverActivity.class));
        		break;
        	        	
        	// Остановить обслуживание
        	case 400:
        		showAlertDialog(1);
    			break;
    		
    		// Проверка оборудования
        	case 500:
        		startActivity(new Intent(this, DeviceCheckActivity.class));
        		break;
        	
        	// Смена пароля
        	case 600:
        		startActivity(new Intent(this, ChangePasswordActivity.class));
        		break;
        	// Отключить брелок
        	case 700:
        		showAlertDialog(2);
        		break;
        }
    }
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		
		int dialogShowing = 0;
		
		if (mExitDialog != null && mExitDialog.isShowing()) {
			dialogShowing = 1;
		}
		else if (mDisconnectDeviceDialog != null && mDisconnectDeviceDialog.isShowing()) {
			dialogShowing = 2;
		}
		
		savedInstanceState.putInt("dialog_showing", dialogShowing);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмники
		unregisterReceiver(mWizardReceiver);
	}
	
	
	
	// Запуск настройки кнопок
	private void startKeySetsActivity() {
		if (MyDevice.hasInternetConnection(this)) {
			HashMap<String, String> params = new HashMap<String, String>();
						
			params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
			params.put("imei", MyDevice.getImei(this));
			params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
			params.put("_to", "modes");
						
			mAsyncTaskFragment.execute("user_modes_get", params, null);
		}
		else {
			Toast.makeText(this, "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
		}
	}
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
	
	// Показ информационного диалога
	private void showAlertDialog(int id) {
		switch (id) {
			case 1:
				mExitDialog = new AlertDialog.Builder(SettingsActivity.this)
            	.setMessage("Вы подтверждаете данное действие?")
            	.setCancelable(true)
            	.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            		public void onClick(DialogInterface dialog, int id) {
                    	dialog.dismiss();
                    	
        	    		if (MyDevice.hasInternetConnection(getApplicationContext())) {
        	    			HashMap<String, String> params = new HashMap<String, String>();
        	    		
        	    			params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
        	    			params.put("imei", MyDevice.getImei(getApplicationContext()));
        	    			params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
        	    			params.put("reason", "2");
        	    			params.put("_to", "logout");
        	    			
        					mAsyncTaskFragment.execute("user_logout", params, null);
        	    		}
        	    		else {
        	    			Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
        	    		}
                	}
            	})
            	.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            		public void onClick(DialogInterface dialog, int id) {
            			dialog.dismiss();
            		}
            	})
            	.create();
			
    			mExitDialog.show();
    			break;
			
			case 2:
				mDisconnectDeviceDialog = new AlertDialog.Builder(SettingsActivity.this)
            	.setMessage("Вы подтверждаете данное действие?")
            	.setCancelable(true)
            	.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            		public void onClick(DialogInterface dialog, int id) {
                    	dialog.dismiss();
                    	
                    	stopService(new Intent(getApplicationContext(), AlarmService.class));
                		startKeySetsActivity();
                	}
            	})
            	.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            		public void onClick(DialogInterface dialog, int id) {
            			dialog.dismiss();
            		}
            	})
            	.create();
			
        		mDisconnectDeviceDialog.show();
        		break;
		}
	}				
}