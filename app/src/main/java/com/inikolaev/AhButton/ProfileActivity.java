package com.inikolaev.AhButton;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity extends FragmentActivity implements AsyncTaskFragment.AsyncTaskCallbacks {

    private static final int IMAGE_REQUEST_1 = 1;
    private static final int IMAGE_REQUEST_2 = 2;
    private static final int IMAGE_REQUEST_3 = 3;
	
    private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	//private EditText mEditLastName;
	//private EditText mEditFirstName;
	//private EditText mEditPatrName;
	//private EditText mEditBirthDate;
	private EditText mEditPhone;
	private EditText mEditCodeword;
	private EditText mEditEmail;
	//private EditText mEditCity;
	//private EditText mEditStreet;
	//private EditText mEditHouse;
	//private EditText mEditBuilding;
	//private EditText mEditApartment;
	//private EditText mEditPassportSerNum;
	//private EditText mEditPassportIssuedBy;
	//private EditText mEditPassportIssueDate;
	
	private Button mButtonPassportPhoto;
	private TextView mTextPassportPhotoLoaded;
	private ImageView mImagePassportPhoto;
	
	private Button mButtonPassportPhotoReg;
	private TextView mTextPassportPhotoRegLoaded;
	private ImageView mImagePassportPhotoReg;
	
	private EditText mEditGuardedLastName;
	private EditText mEditGuardedFirstName;
	private EditText mEditGuardedPatrName;
	private EditText mEditGuardedBirthDate;
	private EditText mEditGuardedPhone;
	//private EditText mEditGuardedCity;
	//private EditText mEditGuardedStreet;
	//private EditText mEditGuardedHouse;
	//private EditText mEditGuardedBuilding;
	//private EditText mEditGuardedApartment;
	private EditText mEditGuardedPhoneContact;
	private EditText mEditGuardedDeseases;
	private Button mButtonGuardedPlaces;
	private Button mButtonTrustedPerson;
	
	private Button mButtonGuardedPhoto;
	private TextView mTextGuardedPhotoLoaded;
	private ImageView mImageGuardedPhoto;
	
	private Button mButtonProfileNext;
	
	private Bitmap mBitmapPassportPhoto;
	private Bitmap mBitmapPassportPhotoReg;
	private Bitmap mBitmapGuardedPhoto;
	
	private String mPassportPhotoUrl;
	private String mPassportPhotoRegUrl;
	private String mGuardedPhotoUrl;
	
	private byte[] mBytesPassportPhoto;
	private byte[] mBytesPassportPhotoReg;
	private byte[] mBytesGuardedPhoto;
	
	private int mHasPassportPhoto;
	private int mHasPassportPhotoReg;
	private int mHasGuardedPhoto;
	
	// Приёмник события закрытия мастера
	private final BroadcastReceiver mWizardReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_WIZARD_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на поля с датами
	private final OnClickListener mEditDatesClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			DatePickerFragment fragment = DatePickerFragment.newInstance();
			
			String date = "";
			OnDateSetListener callback = null;
			
			switch (v.getId()) {
				/*case R.id.editBirthDate:
					date = mEditBirthDate.getText().toString();
					callback = mBirthDateCallback;
					break;*/
				/*case R.id.editPassportIssueDate:
					date = mEditPassportIssueDate.getText().toString();
					callback = mIssueDateCallback;
					break;*/
				case R.id.editGuardedBirthDate:
					date = mEditGuardedBirthDate.getText().toString();
					callback = mGuardedBirthDateCallback;
					break;
			}
			
			String[] parts = date.split("\\.");
			Bundle args = new Bundle();
			
	    	if (parts.length == 3) {
				args.putInt("year", Integer.parseInt(parts[2]));
				args.putInt("month", Integer.parseInt(parts[1])-1);
				args.putInt("day", Integer.parseInt(parts[0]));
			}
			else {
				Calendar calendar = Calendar.getInstance();
				
				args.putInt("year", calendar.get(Calendar.YEAR));
				args.putInt("month", calendar.get(Calendar.MONTH));
				args.putInt("day", calendar.get(Calendar.DAY_OF_MONTH));
			}
			
	    	fragment.setArguments(args);
	    	fragment.setCallback(callback);
	    	fragment.show(getSupportFragmentManager(), "date_picker");
		}
	};
	
	// Слушатель нажатия на изменение поля "Дата рождения"
	/*private OnDateSetListener mBirthDateCallback = new OnDateSetListener() {
    	@Override
    	public void onDateSet(DatePicker view, int year, int month, int day) {
    		mEditBirthDate.setText(buildDateString(year, month, day));
    	}
	};*/

	// Слушатель нажатия на изменения поля "Дата выдачи"
	/*private OnDateSetListener mIssueDateCallback = new OnDateSetListener() {
    	@Override
    	public void onDateSet(DatePicker view, int year, int month, int day) {
    		mEditPassportIssueDate.setText(buildDateString(year, month, day));
    	}
	};*/

	// Слушатель нажатия на изменение поля "Дата рождения охраняемого"
	private OnDateSetListener mGuardedBirthDateCallback = new OnDateSetListener() {
    	@Override
    	public void onDateSet(DatePicker view, int year, int month, int day) {
    		mEditGuardedBirthDate.setText(buildDateString(year, month, day));
    	}
	};
	
	// Слушатель нажатия на кнопку "Фото паспорта (2, 3)"
	private final OnClickListener mButtonPassportPhotoClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	        startActivityForResult(intent, IMAGE_REQUEST_1);
		}
	};
	
	// Слушатель нажатия на кнопку "Фото паспорта (прописка)"
	private final OnClickListener mButtonPassportPhotoRegClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	        startActivityForResult(intent, IMAGE_REQUEST_2);
		}
	};
	
	// Слушатель нажатия на кнопку "Фото охраняемого"
	private final OnClickListener mButtonGuardedPhotoClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	        startActivityForResult(intent, IMAGE_REQUEST_3);
		}
	};
	
	// Слушатель нажатия на кнопку "Места частого пребывания"
	private final OnClickListener mButtonGuardedPlacesClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (MyDevice.hasInternetConnection(getApplicationContext())) {
				HashMap<String, String> params = new HashMap<String, String>();
				
				params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
				params.put("imei", MyDevice.getImei(getApplicationContext()));
				params.put("_to", "places");
				
				mAsyncTaskFragment.execute("user_places_get", params, null);
			}
			else {
				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();	
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Доверенные лица"
	private final OnClickListener mButtonTrustedPersonClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (MyDevice.hasInternetConnection(getApplicationContext())) {
				HashMap<String, String> params = new HashMap<String, String>();
				
				params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
				params.put("imei", MyDevice.getImei(getApplicationContext()));
				params.put("_to", "trusted");
				
				mAsyncTaskFragment.execute("user_trusted_get", params, null);
			}
			else {
				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();	
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Далее"
	private final OnClickListener mButtonProfileNextClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// Проверяем заполненность полей
			if (/*mEditLastName.length() == 0 || mEditFirstName.length() == 0 || mEditPatrName.length() == 0 || 
				mEditBirthDate.length() == 0 ||*/ mEditPhone.length() == 0 || mEditCodeword.length() == 0 /*||
				mEditEmail.length() == 0 ||	mEditCity.length() == 0 || mEditStreet.length() == 0 ||
				mEditHouse.length() == 0 || mEditPassportSerNum.length() == 0 || mEditPassportIssuedBy.length() == 0 ||
				mEditPassportIssueDate.length() == 0 || mEditGuardedLastName.length() == 0 || mEditGuardedFirstName.length() == 0 ||
				mEditGuardedPatrName.length() == 0 || mEditGuardedBirthDate.length() == 0 || mEditGuardedPhone.length() == 0 ||
				mEditGuardedCity.length() == 0 || mEditGuardedStreet.length() == 0 || mEditGuardedHouse.length() == 0 ||
				mEditGuardedPhoneContact.length() == 0*/) {
				Toast.makeText(getApplicationContext(), "Заполните все поля, отмеченные звёздочкой!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// Проверям маски заполнения полей
			/*Calendar now = Calendar.getInstance();
			Calendar birth = Calendar.getInstance();
			
			String[] parts = mEditBirthDate.getText().toString().split("\\.");
			
			birth.set(Integer.valueOf(parts[2]), Integer.valueOf(parts[1]), Integer.valueOf(parts[0]));
			now.add(Calendar.YEAR, -18);
			
			if (now.getTimeInMillis() < birth.getTimeInMillis()) {
				Toast.makeText(getApplicationContext(), "Для заключения договора требуется достижение 18-летнего возраста!", Toast.LENGTH_SHORT).show();
				return;
			}*/
			
			if (!mEditPhone.getText().toString().matches("\\+7[0-9]{10}")) {
				Toast.makeText(getApplicationContext(), "Телефон лица, заключающего договор, должен быть в формате +79991234567!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (mEditEmail.length() > 0 && !mEditEmail.getText().toString().matches("[a-zA-Z0-9._%+-]{2,}@[a-zA-Z0-9.-]{2,}\\.[a-zA-Z]{2,}")) {
				Toast.makeText(getApplicationContext(), "Электронная почта введена некорректно!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			/*if (!mEditPassportSerNum.getText().toString().matches("[0-9]{4}\\s[0-9]{6}")) {
				Toast.makeText(getApplicationContext(), "Серия и номер паспорта должны быть в формате XXXX XXXXXX!", Toast.LENGTH_SHORT).show();
				return;
			}*/
			
			if (mEditGuardedPhone.length() > 0 && !mEditGuardedPhone.getText().toString().matches("\\+7[0-9]{10}")) {
				Toast.makeText(getApplicationContext(), "Телефон охраняемого должен быть в формате +79991234567!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (mEditGuardedPhoneContact.length() > 0 && !mEditGuardedPhoneContact.getText().toString().matches("\\+7[0-9]{10}")) {
				Toast.makeText(getApplicationContext(), "Дополнительный телефон охраняемого должен быть в формате +79991234567!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// Проверяем, загружены ли файлы
			if (mHasPassportPhoto == 0 && mBytesPassportPhoto == null) {			
				Toast.makeText(getApplicationContext(), "Сделайте фото 2, 3 страниц паспорта!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (mHasPassportPhotoReg == 0 && mBytesPassportPhotoReg == null) {			
				Toast.makeText(getApplicationContext(), "Сделайте фото страницы прописки паспорта!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (mHasGuardedPhoto == 0 && mBytesGuardedPhoto == null) {			
				Toast.makeText(getApplicationContext(), "Сделайте фото охраняемого!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// Записываем данные пользователя на сервер
			if (MyDevice.hasInternetConnection(getApplicationContext())) {
				HashMap<String, String> params = new HashMap<String, String>();
				HashMap<String, byte[]> files = new HashMap<String, byte[]>();
				
				params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
				params.put("imei", MyDevice.getImei(getApplicationContext()));
				/*params.put("last_name", mEditLastName.getText().toString());
				params.put("first_name", mEditFirstName.getText().toString());
				params.put("patr_name", mEditPatrName.getText().toString());
				params.put("birth_date", mEditBirthDate.getText().toString());*/
				params.put("phone", mEditPhone.getText().toString());
				params.put("codeword", mEditCodeword.getText().toString());
				params.put("email", mEditEmail.getText().toString());
				/*params.put("city", mEditCity.getText().toString());
				params.put("street", mEditStreet.getText().toString());
				params.put("house", mEditHouse.getText().toString());
				params.put("building", mEditBuilding.getText().toString());
				params.put("apartment", mEditApartment.getText().toString());
				params.put("passport_ser_num", mEditPassportSerNum.getText().toString());
				params.put("passport_issued_by", mEditPassportIssuedBy.getText().toString());
				params.put("passport_issue_date", mEditPassportIssueDate.getText().toString());*/
				params.put("guarded_last_name", mEditGuardedLastName.getText().toString());
				params.put("guarded_first_name", mEditGuardedFirstName.getText().toString());
				params.put("guarded_patr_name", mEditGuardedPatrName.getText().toString());
				params.put("guarded_birth_date", mEditGuardedBirthDate.getText().toString());
				params.put("guarded_phone", mEditGuardedPhone.getText().toString());
				/*params.put("guarded_city", mEditGuardedCity.getText().toString());
				params.put("guarded_street", mEditGuardedStreet.getText().toString());
				params.put("guarded_house", mEditGuardedHouse.getText().toString());
				params.put("guarded_building", mEditGuardedBuilding.getText().toString());
				params.put("guarded_apartment", mEditGuardedApartment.getText().toString());*/
				params.put("guarded_phone_contact", mEditGuardedPhoneContact.getText().toString());
				params.put("guarded_deseases", mEditGuardedDeseases.getText().toString());
				params.put("_to", "profile");
				
				if (mBytesPassportPhoto != null) {
					files.put("passport_photo", mBytesPassportPhoto);
				}
				
				if (mBytesPassportPhotoReg != null) {
					files.put("passport_photo_reg", mBytesPassportPhotoReg);
				}
				
				if (mBytesGuardedPhoto != null) {
					files.put("guarded_photo", mBytesGuardedPhoto);
				}
				
				mAsyncTaskFragment.execute("user_profile", params, files);
			}
			else {
				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();	
			}
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {
		if (mProgressDialog != null) {
			mProgressDialog.setProgress(progress[0]);
		}
	}

	@Override
	public void onCancelled() {}

	@SuppressWarnings("unchecked")
	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.getParam("_to").equals("profile")) {
			if (http.isSuccess()) {
				if (http.getInt("status") > 0) {
					mProgressDialog.dismiss();
					
					if (mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {
						startActivity(new Intent(this, SettingsActivity.class));
					}
					else {
						finish();
					}
				}
				else {
					mProgressDialog.dismiss();
					Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
				}
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
			}
		}
		else if (http.getParam("_to").equals("places")) {
			if (http.isSuccess()) {
				if (http.getInt("status") > 0) {
					mProgressDialog.dismiss();
					
					try {
						HashMap<String, HashMap<String, Object>> places = (HashMap<String, HashMap<String, Object>>)http.getRespMap().get("places");
					
						ArrayList<String> place_cities = new ArrayList<String>();
						ArrayList<String> place_streets = new ArrayList<String>();
						ArrayList<String> place_houses = new ArrayList<String>();
						ArrayList<String> place_buildings = new ArrayList<String>();
						ArrayList<String> place_apartments = new ArrayList<String>();
						ArrayList<String> place_labels = new ArrayList<String>();
						
						String[] tagNames = places.keySet().toArray(new String[places.size()]);
						Arrays.sort(tagNames);
						
						for (int i = 0; i < tagNames.length; i++) {
							place_cities.add((String)places.get(tagNames[i]).get("city"));
							place_streets.add((String)places.get(tagNames[i]).get("street"));
							place_houses.add((String)places.get(tagNames[i]).get("house"));
							place_buildings.add((String)places.get(tagNames[i]).get("building"));
							place_apartments.add((String)places.get(tagNames[i]).get("apartment"));
							place_labels.add((String)places.get(tagNames[i]).get("label"));
						}
						
						Intent intent = new Intent(this, GuardedPlacesActivity.class);
						intent.putStringArrayListExtra("place_cities", place_cities);
						intent.putStringArrayListExtra("place_streets", place_streets);
						intent.putStringArrayListExtra("place_houses", place_houses);
						intent.putStringArrayListExtra("place_buildings", place_buildings);
						intent.putStringArrayListExtra("place_apartments", place_apartments);
						intent.putStringArrayListExtra("place_labels", place_labels);
						
						startActivity(intent);
					}
					catch (Exception e) {
						startActivity(new Intent(this, GuardedPlacesActivity.class));
					}
				}
				else {
					mProgressDialog.dismiss();
					Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
				}
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
			}
		}		
		else if (http.getParam("_to").equals("trusted")) {
			if (http.isSuccess()) {
				if (http.getInt("status") > 0) {
					mProgressDialog.dismiss();
					
					try {
						HashMap<String, HashMap<String, Object>> persons = (HashMap<String, HashMap<String, Object>>)http.getRespMap().get("persons");
					
						ArrayList<String> person_last_names = new ArrayList<String>();
						ArrayList<String> person_first_names = new ArrayList<String>();
						ArrayList<String> person_patr_names = new ArrayList<String>();
						ArrayList<String> person_contact_phones = new ArrayList<String>();
						
						String[] tagNames = persons.keySet().toArray(new String[persons.size()]);
						Arrays.sort(tagNames);
						
						for (int i = 0; i < tagNames.length; i++) {
							person_last_names.add((String)persons.get(tagNames[i]).get("last_name"));
							person_first_names.add((String)persons.get(tagNames[i]).get("first_name"));
							person_patr_names.add((String)persons.get(tagNames[i]).get("patr_name"));
							person_contact_phones.add((String)persons.get(tagNames[i]).get("contact_phone"));
						}
						
						Intent intent = new Intent(this, TrustedPersonsActivity.class);
						intent.putStringArrayListExtra("person_last_names", person_last_names);
						intent.putStringArrayListExtra("person_first_names", person_first_names);
						intent.putStringArrayListExtra("person_patr_names", person_patr_names);
						intent.putStringArrayListExtra("person_contact_phones", person_contact_phones);
						
						startActivity(intent);
					}
					catch (Exception e) {
						startActivity(new Intent(this, TrustedPersonsActivity.class));
				    }
				}
				else {
					mProgressDialog.dismiss();
					Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
				}
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_profile);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    if (mAsyncTaskFragment.isTaskRunning()) {
	    	showProgressDialog();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        // Регистрируем приёмник события закрытия мастера
        registerReceiver(mWizardReceiver, new IntentFilter(AlarmService.ACTION_WIZARD_TERMINATED));
                
	    // Поля ввода
	    //mEditLastName = (EditText)findViewById(R.id.editLastName);
	    //mEditFirstName = (EditText)findViewById(R.id.editFirstName);
	    //mEditPatrName = (EditText)findViewById(R.id.editPatrName);
	    
	    //mEditBirthDate = (EditText)findViewById(R.id.editBirthDate);
	    //mEditBirthDate.setOnClickListener(mEditDatesClick);
	    
	    mEditPhone = (EditText)findViewById(R.id.editPhone);
	    mEditCodeword = (EditText)findViewById(R.id.editCodeword);
	    mEditEmail = (EditText)findViewById(R.id.editEmail);
	    //mEditCity = (EditText)findViewById(R.id.editCity);
	    //mEditStreet = (EditText)findViewById(R.id.editStreet);
	    //mEditHouse = (EditText)findViewById(R.id.editHouse);
	    //mEditBuilding = (EditText)findViewById(R.id.editBuilding);
	    //mEditApartment = (EditText)findViewById(R.id.editApartment);
	    //mEditPassportSerNum = (EditText)findViewById(R.id.editPassportSerNum);
	    //mEditPassportIssuedBy = (EditText)findViewById(R.id.editPassportIssuedBy);
	    
	    //mEditPassportIssueDate = (EditText)findViewById(R.id.editPassportIssueDate);
	    //mEditPassportIssueDate.setOnClickListener(mEditDatesClick);
	    
	    mTextPassportPhotoLoaded = (TextView)findViewById(R.id.textPassportPhotoLoaded);
	    mImagePassportPhoto = (ImageView)findViewById(R.id.imagePassportPhoto);
	    mTextPassportPhotoRegLoaded = (TextView)findViewById(R.id.textPassportPhotoRegLoaded);
	    mImagePassportPhotoReg = (ImageView)findViewById(R.id.imagePassportPhotoReg);
	    mEditGuardedLastName = (EditText)findViewById(R.id.editGuardedLastName);
	    mEditGuardedFirstName = (EditText)findViewById(R.id.editGuardedFirstName);
	    mEditGuardedPatrName = (EditText)findViewById(R.id.editGuardedPatrName);
	    
	    mEditGuardedBirthDate = (EditText)findViewById(R.id.editGuardedBirthDate);
	    mEditGuardedBirthDate.setOnClickListener(mEditDatesClick);
	    
	    mEditGuardedPhone = (EditText)findViewById(R.id.editGuardedPhone);
	    //mEditGuardedCity = (EditText)findViewById(R.id.editGuardedCity);
	    //mEditGuardedStreet = (EditText)findViewById(R.id.editGuardedStreet);
	    //mEditGuardedHouse = (EditText)findViewById(R.id.editGuardedHouse);
	    //mEditGuardedBuilding = (EditText)findViewById(R.id.editGuardedBuilding);
	    //mEditGuardedApartment = (EditText)findViewById(R.id.editGuardedApartment);
	    mEditGuardedPhoneContact = (EditText)findViewById(R.id.editGuardedPhoneContact);
	    mEditGuardedDeseases = (EditText)findViewById(R.id.editGuardedDeseases);
	    mTextGuardedPhotoLoaded = (TextView)findViewById(R.id.textGuardedPhotoLoaded);
	    mImageGuardedPhoto = (ImageView)findViewById(R.id.imageGuardedPhoto);
	    
	    // Кнопка "Фото паспорта (2, 3)"
	    mButtonPassportPhoto = (Button)findViewById(R.id.buttonPassportPhoto);
	    mButtonPassportPhoto.setOnClickListener(mButtonPassportPhotoClick);
	    
	    // Кнопка "Фото паспорта (прописка)"
	    mButtonPassportPhotoReg = (Button)findViewById(R.id.buttonPassportPhotoReg);
	    mButtonPassportPhotoReg.setOnClickListener(mButtonPassportPhotoRegClick);
	    
	    // Кнопка "Места частого пребывания"
	    mButtonGuardedPlaces = (Button)findViewById(R.id.buttonGuardedPlaces);
	    mButtonGuardedPlaces.setOnClickListener(mButtonGuardedPlacesClick);
	    
	    // Кнопка "Доверенные лица"
	    mButtonTrustedPerson = (Button)findViewById(R.id.buttonTrustedPerson);
	    mButtonTrustedPerson.setOnClickListener(mButtonTrustedPersonClick);
	    
	    // Кнопка "Фото охраняемого"
	    mButtonGuardedPhoto = (Button)findViewById(R.id.buttonGuardedPhoto);
	    mButtonGuardedPhoto.setOnClickListener(mButtonGuardedPhotoClick);
	    
	    // Кнопка "Далее"
	    mButtonProfileNext = (Button)findViewById(R.id.buttonProfileNext);
	    mButtonProfileNext.setOnClickListener(mButtonProfileNextClick);
	    
	    // Автозаполняем данные формы
	    Intent intent = getIntent();
    	
    	/*String lastName = intent.getStringExtra("last_name");
    	if (lastName != null) {
    		mEditLastName.setText(lastName);
    	}
    	
    	String firstName = intent.getStringExtra("first_name");
    	if (firstName != null) {
    		mEditFirstName.setText(firstName);
    	}
    	
    	String patrName = intent.getStringExtra("patr_name");
    	if (patrName != null) {
    		mEditPatrName.setText(patrName);
    	}
    	
    	String birthDate = intent.getStringExtra("birth_date");
    	if (birthDate != null) {
    		mEditBirthDate.setText(birthDate);
    	}*/
    	
    	String phone = intent.getStringExtra("phone");
    	if (phone != null) {
    		mEditPhone.setText(phone);
    	}
    	
    	String codeword = intent.getStringExtra("codeword");
    	if (codeword != null) {
    		mEditCodeword.setText(codeword);
    	}
    	
    	String email = intent.getStringExtra("email");
    	if (email != null) {
    		mEditEmail.setText(email);
    	}
    	
    	/*String city = intent.getStringExtra("city");
    	if (city != null) {
    		mEditCity.setText(city);
    	}
    	
    	String street = intent.getStringExtra("street");
    	if (street != null) {
    		mEditStreet.setText(street);
    	}
    	
    	String house = intent.getStringExtra("house");
    	if (house != null) {
    		mEditHouse.setText(house);
    	}
    	
    	String building = intent.getStringExtra("building");
    	if (building != null) {
    		mEditBuilding.setText(building);
    	}
    	
    	String apartment = intent.getStringExtra("apartment");
    	if (apartment != null) {
    		mEditApartment.setText(apartment);
    	}
    	
    	String passportSerNum = intent.getStringExtra("passport_ser_num");
    	if (passportSerNum != null) {
    		mEditPassportSerNum.setText(passportSerNum);
    	}
    	
    	String passportIssuedBy = intent.getStringExtra("passport_issued_by");
    	if (passportIssuedBy != null) {
    		mEditPassportIssuedBy.setText(passportIssuedBy);
    	}
    	
    	String passportIssueDate = intent.getStringExtra("passport_issue_date");
    	if (passportIssueDate != null) {
    		mEditPassportIssueDate.setText(passportIssueDate);
    	}*/
    	
    	mHasPassportPhoto = intent.getIntExtra("passport_photo_status", 0);
    	if (mHasPassportPhoto == 1) {
    		mTextPassportPhotoLoaded.setVisibility(View.VISIBLE);
    		
    		mPassportPhotoUrl = intent.getStringExtra("passport_photo_url");
    		if (mPassportPhotoUrl != null && mPassportPhotoUrl.length() > 0) {
    			mTextPassportPhotoLoaded.setClickable(true);
    			mTextPassportPhotoLoaded.setMovementMethod(LinkMovementMethod.getInstance());
    			mTextPassportPhotoLoaded.setText(Html.fromHtml("<a href='" + mPassportPhotoUrl + "'>Посмотреть загруженное фото</a>"));
    		}
    	}
    	
    	mHasPassportPhotoReg = intent.getIntExtra("passport_photo_reg_status", 0);
    	if (mHasPassportPhotoReg == 1) {
    		mTextPassportPhotoRegLoaded.setVisibility(View.VISIBLE);
    		
    		mPassportPhotoRegUrl = intent.getStringExtra("passport_photo_reg_url");
    		if (mPassportPhotoRegUrl != null && mPassportPhotoRegUrl.length() > 0) {
    			mTextPassportPhotoRegLoaded.setClickable(true);
    			mTextPassportPhotoRegLoaded.setMovementMethod(LinkMovementMethod.getInstance());
    			mTextPassportPhotoRegLoaded.setText(Html.fromHtml("<a href='" + mPassportPhotoRegUrl + "'>Посмотреть загруженное фото</a>"));
    		}
    	}
    	
    	String guardedLastName = intent.getStringExtra("guarded_last_name");
    	if (guardedLastName != null) {
    		mEditGuardedLastName.setText(guardedLastName);
    	}
    	
    	String guardedFirstName = intent.getStringExtra("guarded_first_name");
    	if (guardedFirstName != null) {
    		mEditGuardedFirstName.setText(guardedFirstName);
    	}
    	
    	String guardedPatrName = intent.getStringExtra("guarded_patr_name");
    	if (guardedPatrName != null) {
    		mEditGuardedPatrName.setText(guardedPatrName);
    	}
    	
    	String guardedBirthDate = intent.getStringExtra("guarded_birth_date");
    	if (guardedBirthDate != null) {
    		mEditGuardedBirthDate.setText(guardedBirthDate);
    	}
    	
    	String guardedPhone = intent.getStringExtra("guarded_phone");
    	if (guardedPhone != null) {
    		mEditGuardedPhone.setText(guardedPhone);
    	}
    	
    	/*String guardedCity = intent.getStringExtra("guarded_city");
    	if (guardedCity != null) {
    		mEditGuardedCity.setText(guardedCity);
    	}
    	
    	String guardedStreet = intent.getStringExtra("guarded_street");
    	if (guardedStreet != null) {
    		mEditGuardedStreet.setText(guardedStreet);
    	}
    	
    	String guardedHouse = intent.getStringExtra("guarded_house");
    	if (guardedHouse != null) {
    		mEditGuardedHouse.setText(guardedHouse);
    	}
    	
    	String guardedBuilding = intent.getStringExtra("guarded_building");
    	if (guardedBuilding != null) {
    		mEditGuardedBuilding.setText(guardedBuilding);
    	}
    	
    	String guardedApartment = intent.getStringExtra("guarded_apartment");
    	if (guardedApartment != null) {
    		mEditGuardedApartment.setText(guardedApartment);
    	}*/
    	
    	String guardedPhoneContact = intent.getStringExtra("guarded_phone_contact");
    	if (guardedPhoneContact != null) {
    		mEditGuardedPhoneContact.setText(guardedPhoneContact);
    	}
    	
    	String guardedDeseases = intent.getStringExtra("guarded_deseases");
    	if (guardedDeseases != null) {
    		mEditGuardedDeseases.setText(guardedDeseases);
    	}
    	
    	mHasGuardedPhoto = intent.getIntExtra("guarded_photo_status", 0);
    	if (mHasGuardedPhoto == 1) {
    		mTextGuardedPhotoLoaded.setVisibility(View.VISIBLE);
    		
    		mGuardedPhotoUrl = intent.getStringExtra("guarded_photo_url");
    		if (mGuardedPhotoUrl != null && mGuardedPhotoUrl.length() > 0) {
    			mTextGuardedPhotoLoaded.setClickable(true);
    			mTextGuardedPhotoLoaded.setMovementMethod(LinkMovementMethod.getInstance());
    			mTextGuardedPhotoLoaded.setText(Html.fromHtml("<a href='" + mGuardedPhotoUrl + "'>Посмотреть загруженное фото</a>"));
    		}
    	}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if (!mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {
			mButtonProfileNext.setText(R.string.save);
		}
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) { 
		super.onActivityResult(requestCode, resultCode, data);
		
        if (resultCode == RESULT_OK && data != null) {
        	ByteArrayOutputStream stream = new ByteArrayOutputStream();
        		
        	Uri pickedImage = data.getData();
        	String[] filePath = { MediaStore.Images.Media.DATA };
        		
        	Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
        	cursor.moveToFirst();
        	
        	String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
        	
        	switch (requestCode) {
        		case IMAGE_REQUEST_1:
        			mBitmapPassportPhoto = BitmapFactory.decodeFile(imagePath);
        			if (mBitmapPassportPhoto == null) {
                		Toast.makeText(this, "Выбор изображения из этой папки невозможен!", Toast.LENGTH_SHORT).show();
                		return;        	
                	}
        			
        			mImagePassportPhoto.setImageBitmap(mBitmapPassportPhoto);
        			mImagePassportPhoto.setVisibility(View.VISIBLE);
        			mTextPassportPhotoLoaded.setVisibility(View.GONE);
        			
        			mBitmapPassportPhoto.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        			mBytesPassportPhoto = stream.toByteArray();
        			break;
        			
        		case IMAGE_REQUEST_2:
        			mBitmapPassportPhotoReg = BitmapFactory.decodeFile(imagePath);
        			if (mBitmapPassportPhotoReg == null) {
                		Toast.makeText(this, "Выбор изображения из этой папки невозможен!", Toast.LENGTH_SHORT).show();
                		return;        	
                	}
        			
        			mImagePassportPhotoReg.setImageBitmap(mBitmapPassportPhotoReg);
        			mImagePassportPhotoReg.setVisibility(View.VISIBLE);
        			mTextPassportPhotoRegLoaded.setVisibility(View.GONE);
        			
        			mBitmapPassportPhotoReg.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        			mBytesPassportPhotoReg = stream.toByteArray();
        			break;
        			
        		case IMAGE_REQUEST_3:
        			mBitmapGuardedPhoto = BitmapFactory.decodeFile(imagePath);
        			if (mBitmapGuardedPhoto == null) {
                		Toast.makeText(this, "Выбор изображения из этой папки невозможен!", Toast.LENGTH_SHORT).show();
                		return;        	
                	}
        			
        			mImageGuardedPhoto.setImageBitmap(mBitmapGuardedPhoto);
        			mImageGuardedPhoto.setVisibility(View.VISIBLE);
        			mTextGuardedPhotoLoaded.setVisibility(View.GONE);
        			
        			mBitmapGuardedPhoto.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        			mBytesGuardedPhoto = stream.toByteArray();
        			break;
        	}
        	cursor.close();
        }
    }
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмник
		unregisterReceiver(mWizardReceiver);
	}
	
	
	
	// Построение строки даты с ведущими нулями
	private String buildDateString(int year, int month, int day) {
		String sDay = Integer.toString(day);
  		String sMonth = Integer.toString(month+1);
		String sYear = Integer.toString(year);
  		
  		if (sDay.length() == 1) sDay = "0" + sDay;
  		if (sMonth.length() == 1) sMonth = "0" + sMonth;
  		
  		return sDay + "." + sMonth + "." + sYear;
	}
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		
		if (mBytesPassportPhoto != null || mBytesPassportPhotoReg != null || mBytesGuardedPhoto != null) {
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.setMax(100);
		}
		
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}
