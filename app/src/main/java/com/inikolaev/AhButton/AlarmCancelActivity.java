package com.inikolaev.AhButton;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AlarmCancelActivity extends FragmentActivity implements AsyncTaskFragment.AsyncTaskCallbacks {
	
	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private EditText mEditPasswordAlarmCancel;
	private Button mButtonAlarmCancelSubmit;
	
	private int mAlarmId = 0;

	// Приёмник событий тревоги
	private final BroadcastReceiver mAlarmReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_ALARM_FINISHED.equals(action)) {
				finish();
			}
		}
	};
	
	// Приёмник события закрытия окна
	private final BroadcastReceiver mStopReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_APP_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Отменить тревогу"
	@SuppressLint("SimpleDateFormat")
	private final OnClickListener mButtonAlarmCancelSubmitClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// Проверка ввода пароля
			if (mEditPasswordAlarmCancel.length() < 5) {
				Toast.makeText(getApplicationContext(), "Длина пароля должна быть не менее 5 символов.", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// Проверка числа попыток
			if (mSettings.getInt(MyDevice.APP_PREFS_ATTEMPTS, 0) == 0) {
				Toast.makeText(getApplicationContext(), "Количество попыток закончилось!\nТревога не отменена.", Toast.LENGTH_SHORT).show();
				finish();
				
				return;
			}
			
			// Устанавливаем отмену тревоги сервере
			if (MyDevice.hasInternetConnection(getApplicationContext())) {
				HashMap<String, String> params = new HashMap<String, String>();
								
				params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
				params.put("imei", MyDevice.getImei(getApplicationContext()));
				params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
				params.put("datetime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				params.put("alarm_id", Integer.toString(mAlarmId));
				params.put("password", mEditPasswordAlarmCancel.getText().toString());
				
				mAsyncTaskFragment.execute("alarm_cancel", params, null);
			}
			else {
				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				mProgressDialog.dismiss();
				
				sendBroadcast(new Intent(AlarmService.ACTION_ALARM_CANCELED));
				Toast.makeText(getApplicationContext(), "Тревога отменена!", Toast.LENGTH_SHORT).show();
			}
			else {
				int attempts = mSettings.getInt(MyDevice.APP_PREFS_ATTEMPTS, 0) - 1;
				
				Editor editor = mSettings.edit();
				editor.putInt(MyDevice.APP_PREFS_ATTEMPTS, attempts);
				editor.apply();
				
				mProgressDialog.dismiss();
				Toast.makeText(getApplicationContext(), http.getString("error") + "\nОсталось попыток: " + Integer.toString(attempts), Toast.LENGTH_SHORT).show();
			}
		}
		else {
			mProgressDialog.dismiss();
			Toast.makeText(getApplicationContext(), "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_alarm_cancel);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    if (mAsyncTaskFragment.isTaskRunning()) {
	    	showProgressDialog();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        // Регистрируем приёмник событий AlarmService
        registerReceiver(mAlarmReceiver, new IntentFilter(AlarmService.ACTION_ALARM_FINISHED));
        
	    // Регистрируем приёмник события закрытия окна
	    registerReceiver(mStopReceiver, new IntentFilter(AlarmService.ACTION_APP_TERMINATED));
        
	    // Поле пароля
	    mEditPasswordAlarmCancel = (EditText)findViewById(R.id.editPasswordAlarmCancel);
	    
	    // Кнопка "Отменить тревогу"
	    mButtonAlarmCancelSubmit = (Button)findViewById(R.id.buttonAlarmCancelSubmit);
	    mButtonAlarmCancelSubmit.setOnClickListener(mButtonAlarmCancelSubmitClick);
	    
	    // Получаем данные
	    if (savedInstanceState != null) {
	    	mAlarmId = savedInstanceState.getInt("alarm_id");
	    }
	    else mAlarmId = getIntent().getIntExtra("alarm_id", 0);
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		
		savedInstanceState.putInt("alarm_id", mAlarmId);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмники
		unregisterReceiver(mAlarmReceiver);
		unregisterReceiver(mStopReceiver);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}
