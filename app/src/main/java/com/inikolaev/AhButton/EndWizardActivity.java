package com.inikolaev.AhButton;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class EndWizardActivity extends Activity {

	private SharedPreferences mSettings;
	
	private Button mButtonFinish;
	
	// Приёмник события закрытия мастера
	private final BroadcastReceiver mWizardReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_WIZARD_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Готово"
	private final OnClickListener mButtonFinishClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Editor editor = mSettings.edit();
			editor.putBoolean(MyDevice.APP_PREFS_REGISTERING, false);
			editor.putBoolean(MyDevice.APP_PREFS_TARIFF_SET, false);
			editor.apply();
			
			sendBroadcast(new Intent(AlarmService.ACTION_WIZARD_TERMINATED));
			
			startActivity(new Intent(getApplicationContext(), MainActivity.class));
			finish();
		}
	};
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_end_wizard);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
	    // Регистрируем приёмник события закрытия мастера
	    registerReceiver(mWizardReceiver, new IntentFilter(AlarmService.ACTION_APP_TERMINATED));
	    
	    // Кнопка "Готово"
	    mButtonFinish = (Button)findViewById(R.id.buttonFinish);
	    mButtonFinish.setOnClickListener(mButtonFinishClick);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмник
		unregisterReceiver(mWizardReceiver);
	}
}
