package com.inikolaev.AhButton;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ChangePasswordActivity extends FragmentActivity implements AsyncTaskFragment.AsyncTaskCallbacks {
	
	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private EditText mEditNowPassword;
	private EditText mEditChangePassword;
	private EditText mEditChangePasswordConfirm;
	private Button mButtonChangePassword;

	// Приёмник события закрытия окна
	private final BroadcastReceiver mStopReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_APP_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Сменить пароль"
	private final OnClickListener mButtonChangePasswordClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// Проверяем введённые данные
			if (mEditNowPassword.length() < 5 || mEditChangePassword.length() < 5 || mEditChangePasswordConfirm.length() < 5) {
				Toast.makeText(getApplicationContext(), "Длина пароля должна быть не менее 5 символов!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (!mEditChangePassword.getText().toString().equals(mEditChangePasswordConfirm.getText().toString())) {
				Toast.makeText(getApplicationContext(), "Новые пароли должны совпадать!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// Отправляем запрос на сервер
			if (MyDevice.hasInternetConnection(getApplicationContext())) {
				HashMap<String, String> params = new HashMap<String, String>();
								
				params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
				params.put("password", mEditNowPassword.getText().toString());
				params.put("new_password", mEditChangePassword.getText().toString());
				
				mAsyncTaskFragment.execute("user_password_change", params, null);
			}
			else {
				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				Editor editor = mSettings.edit();
				editor.putString(MyDevice.APP_PREFS_PASSWORD, mEditChangePassword.getText().toString());
				editor.apply();
				
				mProgressDialog.dismiss();
				
				finish();
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(getApplicationContext(), http.getString("error"), Toast.LENGTH_SHORT).show();
			}
		}
		else {
			mProgressDialog.dismiss();
			Toast.makeText(getApplicationContext(), "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
		}
	}
	
		
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_change_password);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    if (mAsyncTaskFragment.isTaskRunning()) {
	    	showProgressDialog();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
	    
	    // Регистрируем приёмник события закрытия окна
	    registerReceiver(mStopReceiver, new IntentFilter(AlarmService.ACTION_APP_TERMINATED));
	    
	    // Поля ввода
        mEditNowPassword = (EditText)findViewById(R.id.editNowPassword);
        mEditChangePassword = (EditText)findViewById(R.id.editChangePassword);
        mEditChangePasswordConfirm = (EditText)findViewById(R.id.editChangePasswordConfirm);
	    
	    // Кнопка "Сменить пароль"
	    mButtonChangePassword= (Button)findViewById(R.id.buttonChangePassword);
	    mButtonChangePassword.setOnClickListener(mButtonChangePasswordClick);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмник
		unregisterReceiver(mStopReceiver);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}