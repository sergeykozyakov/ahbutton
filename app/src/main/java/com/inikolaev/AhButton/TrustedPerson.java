package com.inikolaev.AhButton;

import android.os.Parcel;
import android.os.Parcelable;

public class TrustedPerson implements Parcelable {
	public String lastName;
	public String firstName;
	public String patrName;
	public String contactPhone;
	
	public TrustedPerson(String lastName, String firstName, String patrName, String contactPhone) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.patrName = patrName;
		this.contactPhone = contactPhone;
	}
	
	public TrustedPerson(Parcel in) {
		String[] data = new String[4];
		in.readStringArray(data);
		
		this.lastName = data[0];
		this.firstName = data[1];
		this.patrName = data[2];
		this.contactPhone = data[3];
    }

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStringArray(new String[] {this.lastName, this.firstName, this.patrName, this.contactPhone});
	}
	
	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public TrustedPerson createFromParcel(Parcel in) {
			return new TrustedPerson(in); 
		}

		public TrustedPerson[] newArray(int size) {
			return new TrustedPerson[size];
		}
	};
}