package com.inikolaev.AhButton;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

public class TrustedPersonsActivity extends FragmentListActivity implements AsyncTaskFragment.AsyncTaskCallbacks {

	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private TrustedPersonsAdapter mTrustedPersonsAdapter;
	
	private ArrayList<String> mPersonLastNames;
	private ArrayList<String> mPersonFirstNames;
	private ArrayList<String> mPersonPatrNames;
	private ArrayList<String> mPersonContactPhones;
	
	private Button mButtonTrustedPersonsNext;
	private Button mButtonTrustedPersonsAdd;
	
	// Приёмник события закрытия мастера
	private final BroadcastReceiver mWizardReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_WIZARD_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Сохранить"
	private final OnClickListener mButtonTrustedPersonsNextClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (MyDevice.hasInternetConnection(getApplicationContext())) {
				HashMap<String, String> params = new HashMap<String, String>();
				
				params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
				params.put("imei", MyDevice.getImei(getApplicationContext()));
				
				for (int i = 0; i < mTrustedPersonsAdapter.getCount(); i++) {
					params.put("last_name[" + i +"]", mTrustedPersonsAdapter.getItem(i).lastName);
					params.put("first_name[" + i +"]", mTrustedPersonsAdapter.getItem(i).firstName);
					params.put("patr_name[" + i +"]", mTrustedPersonsAdapter.getItem(i).patrName);
					params.put("contact_phone[" + i +"]", mTrustedPersonsAdapter.getItem(i).contactPhone);
				}
				
				mAsyncTaskFragment.execute("user_trusted_set", params, null);
			}
			else {
				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Добавить запись"
	private final OnClickListener mButtonTrustedPersonsAddClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			showEditDialog(-1);
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				mProgressDialog.dismiss();
				
				finish();
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
			}
		}
		else {
			mProgressDialog.dismiss();
			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_trusted_persons);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        // Регистрируем приёмник события закрытия
        registerReceiver(mWizardReceiver, new IntentFilter(AlarmService.ACTION_WIZARD_TERMINATED));
        
        // Кнопка "Сохранить"
	    mButtonTrustedPersonsNext = (Button)findViewById(R.id.buttonTrustedPersonsNext);
	    mButtonTrustedPersonsNext.setOnClickListener(mButtonTrustedPersonsNextClick);
	    
	    // Кнопка "Добавить запись"
	    mButtonTrustedPersonsAdd = (Button)findViewById(R.id.buttonTrustedPersonsAdd);
	    mButtonTrustedPersonsAdd.setOnClickListener(mButtonTrustedPersonsAddClick);
	    
	    // Заполняем список доверенных лиц
	    Intent intent = getIntent();
	    
	    mPersonLastNames = intent.getStringArrayListExtra("person_last_names");
	    if (mPersonLastNames == null) {
	    	mPersonLastNames = new ArrayList<String>();
	    }
	    
	    mPersonFirstNames = intent.getStringArrayListExtra("person_first_names");
	    if (mPersonFirstNames == null) {
	    	mPersonFirstNames = new ArrayList<String>();
	    }
	    
	    mPersonPatrNames = intent.getStringArrayListExtra("person_patr_names");
	    if (mPersonPatrNames == null) {
	    	mPersonPatrNames = new ArrayList<String>();
	    }
	    
	    mPersonContactPhones = intent.getStringArrayListExtra("person_contact_phones");
	    if (mPersonContactPhones == null) {
	    	mPersonContactPhones = new ArrayList<String>();
	    }
	    
	    ArrayList<TrustedPerson> persons = new ArrayList<TrustedPerson>();
	    
	    for (int i = 0; i < mPersonLastNames.size(); i++) {
	    	persons.add(new TrustedPerson(mPersonLastNames.get(i), mPersonFirstNames.get(i), mPersonPatrNames.get(i), mPersonContactPhones.get(i)));
	    }
	    
	    mTrustedPersonsAdapter = new TrustedPersonsAdapter(this, R.layout.list_trusted_person, persons);
	    setListAdapter(mTrustedPersonsAdapter);
	}
	
	@Override
	protected int getSupportLayoutResourceId() {
		return R.layout.activity_trusted_persons;
	}
	
	@Override
    protected void onListItemClick(ListView l, View v, final int position, long id) {
		showEditDialog(position);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмники
		unregisterReceiver(mWizardReceiver);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
	
	// Показ диалога редактирования
	@SuppressLint("InflateParams")
	private void showEditDialog(final int position) {
		ScrollView view = (ScrollView)getLayoutInflater().inflate(R.layout.edit_trusted_person, null);
		final EditText lastName = (EditText)view.findViewById(R.id.editTrustedPersonLastName);
		final EditText firstName = (EditText)view.findViewById(R.id.editTrustedPersonFirstName);
		final EditText patrName = (EditText)view.findViewById(R.id.editTrustedPersonPatrName);
		final EditText contactPhone = (EditText)view.findViewById(R.id.editTrustedPersonContactPhone);
		
		if (position > -1) {
			lastName.setText(mTrustedPersonsAdapter.getItem(position).lastName);
			firstName.setText(mTrustedPersonsAdapter.getItem(position).firstName);
			patrName.setText(mTrustedPersonsAdapter.getItem(position).patrName);
			contactPhone.setText(mTrustedPersonsAdapter.getItem(position).contactPhone);
		}
		
		final AlertDialog editDialog = new AlertDialog.Builder(TrustedPersonsActivity.this)
    	.setMessage("Введите данные доверенного лица")
    	.setCancelable(true)
    	.setView(view)
    	.setPositiveButton("Сохранить", null)
    	.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int id) {
    			dialog.dismiss();
    		}
    	})
    	.create();
	
		editDialog.show();
		
		editDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (lastName.length() == 0 || firstName.length() == 0 || patrName.length() == 0 || contactPhone.length() == 0) {
					Toast.makeText(getApplicationContext(), "Заполните все поля, отмеченные звёздочкой!", Toast.LENGTH_SHORT).show();
					return;
				}
				
				if (!contactPhone.getText().toString().matches("\\+7[0-9]{10}")) {
					Toast.makeText(getApplicationContext(), "Контактный телефон должен быть в формате +79991234567!", Toast.LENGTH_SHORT).show();
					return;
				}
				
				if (position == -1) {
					mTrustedPersonsAdapter.add(new TrustedPerson(lastName.getText().toString(), firstName.getText().toString(), 
						patrName.getText().toString(), contactPhone.getText().toString()));
				}
				else {
					mTrustedPersonsAdapter.remove(mTrustedPersonsAdapter.getItem(position));
					mTrustedPersonsAdapter.insert(new TrustedPerson(lastName.getText().toString(), firstName.getText().toString(),
						patrName.getText().toString(), contactPhone.getText().toString()), position);
				}
				
				editDialog.dismiss();
			}
		});
	}
}
