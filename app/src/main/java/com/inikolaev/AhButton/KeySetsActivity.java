package com.inikolaev.AhButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class KeySetsActivity extends FragmentActivity implements AsyncTaskFragment.AsyncTaskCallbacks {

	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private Spinner mSpinnerModesConfs;
	private EditText mEditLabel1;
	private Spinner mSpinnerConfirmations1;
	private EditText mEditLabel2;
	private Spinner mSpinnerConfirmations2;
	private Button mButtonKeySetsNext;
	
	private Vibrator mVibrator;
	private MediaPlayer mMediaPlayer;
	
	private ArrayAdapter<CharSequence> mAdapterModeConfs1;
	private ArrayAdapter<CharSequence> mAdapterModeConfs2;
	
	private boolean mIsFirstConfSelected = true;
	
	private int mConfirmation1; 
	private int mConfirmation2;
	
	// Приёмник события закрытия мастера
	private final BroadcastReceiver mWizardReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_WIZARD_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель выбора режима подтверждений
	private final OnItemSelectedListener mSpinnerModesConfsSelect = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
			if (position == 1) {
				mSpinnerConfirmations1.setAdapter(mAdapterModeConfs2);
				mSpinnerConfirmations2.setAdapter(mAdapterModeConfs2);
			}
			else {
				mSpinnerConfirmations1.setAdapter(mAdapterModeConfs1);
				mSpinnerConfirmations2.setAdapter(mAdapterModeConfs1);
			}
			
			if (mIsFirstConfSelected) {
				mSpinnerConfirmations1.setSelection(mConfirmation1, false);
				mSpinnerConfirmations2.setSelection(mConfirmation2, false);
				
				mSpinnerConfirmations1.setOnItemSelectedListener(mSpinnerConfirmationsNSelect);
				mSpinnerConfirmations2.setOnItemSelectedListener(mSpinnerConfirmationsNSelect);
				
				mIsFirstConfSelected = false;
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {}	
	};
	
	// Слушатель выбора подтверждения
	private final OnItemSelectedListener mSpinnerConfirmationsNSelect = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
			switch (position) {
    			case 0:
    				break;
    			case 1:
    				if (mVibrator != null) {
    					mVibrator.vibrate(new long[] {0, 100, 100, 1000}, -1);
    				}
    				break;
    			case 2:
    				if (mVibrator != null) {
    					mVibrator.vibrate(new long[] {0, 100, 100, 1000, 1000, 100, 100, 1000}, -1);
    				}
    				break;
    			case 3:
    				if (mVibrator != null) {
    					mVibrator.vibrate(new long[] {0, 2000, 100, 2000, 100, 2000}, -1);
    				}
    				break;
    			case 4:
    				playSound(R.raw.sound1);
    				break;
    			case 5:
    				playSound(R.raw.sound2);
    				break;
    			case 6:
    				playSound(R.raw.sound3);
    				break;
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {}
	};
		
	// Слушатель нажатия на кнопку "Далее"
	private final OnClickListener mButtonKeySetsNextClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// Проверяем введённые данные
			if (mEditLabel1.length() == 0 || mEditLabel2.length() == 0) {
				Toast.makeText(getApplicationContext(), "Введите наименования кнопок!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// Логинимся на сервере
			if (MyDevice.hasInternetConnection(getApplicationContext())) {
				HashMap<String, String> params = new HashMap<String, String>();
				
				params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
				params.put("imei", MyDevice.getImei(getApplicationContext()));
				params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
				params.put("mode_confs", Integer.toString(mSpinnerModesConfs.getSelectedItemPosition()));
				params.put("label_1", mEditLabel1.getText().toString());
				params.put("confirmation_1", Integer.toString(mSpinnerConfirmations1.getSelectedItemPosition()));
				params.put("label_2", mEditLabel2.getText().toString());
				params.put("confirmation_2", Integer.toString(mSpinnerConfirmations2.getSelectedItemPosition()));
				
				mAsyncTaskFragment.execute("user_modes_set", params, null);
			}
			else {
				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@SuppressWarnings("unchecked")
	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				Editor editor = mSettings.edit();
				editor.putInt(MyDevice.APP_PREFS_MODE_CONFS, mSpinnerModesConfs.getSelectedItemPosition());
				editor.putString(MyDevice.APP_PREFS_LABEL_1, mEditLabel1.getText().toString());
				editor.putInt(MyDevice.APP_PREFS_CONFIRMATION_1, mSpinnerConfirmations1.getSelectedItemPosition());
				editor.putString(MyDevice.APP_PREFS_LABEL_2, mEditLabel2.getText().toString());
				editor.putInt(MyDevice.APP_PREFS_CONFIRMATION_2, mSpinnerConfirmations2.getSelectedItemPosition());
				editor.apply();
				
				HashMap<String, HashMap<String, Object>> tariffs = (HashMap<String, HashMap<String, Object>>)http.getRespMap().get("tariffs");
				
				int tariff_ind = 0;
				ArrayList<String> tariff_ids = new ArrayList<String>();
				ArrayList<String> tariff_names = new ArrayList<String>();
				ArrayList<String> tariff_descs = new ArrayList<String>();
				
				String[] tagNames = tariffs.keySet().toArray(new String[tariffs.size()]);
				Arrays.sort(tagNames);
				
				for (int i = 0; i < tagNames.length; i++) {
					if (((String)tariffs.get(tagNames[i]).get("id")).equals(http.getString("tariff_now")) && 
						!(http.getString("tariff_now")).equals("0")) {
						tariff_ind = i;
					}
					
					tariff_ids.add((String)tariffs.get(tagNames[i]).get("id"));
					tariff_names.add((String)tariffs.get(tagNames[i]).get("name"));
					tariff_descs.add((String)tariffs.get(tagNames[i]).get("desc"));
				}
				
				mProgressDialog.dismiss();
				
				if (mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {
					Intent intent = new Intent(this, TariffActivity.class);
					intent.putExtra("tariff", tariff_ind);
					intent.putStringArrayListExtra("tariff_ids", tariff_ids);
					intent.putStringArrayListExtra("tariff_names", tariff_names);
					intent.putStringArrayListExtra("tariff_descs", tariff_descs);
					
					startActivity(intent);
				}
				else {
					finish();
				}
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
			}
		}
		else {
			mProgressDialog.dismiss();
			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_key_sets);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    if (mAsyncTaskFragment.isTaskRunning()) {
	    	showProgressDialog();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        // Регистрируем приёмник события закрытия мастера
        registerReceiver(mWizardReceiver, new IntentFilter(AlarmService.ACTION_WIZARD_TERMINATED));
        
        // Подключаем вибратор
        mVibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
    	
        // Выбор режима подтверждения
        mSpinnerModesConfs = (Spinner)findViewById(R.id.spinnerModesConfs);
        mSpinnerModesConfs.setOnItemSelectedListener(mSpinnerModesConfsSelect);
                
        // Адаптеры режимов подтверждения
        mAdapterModeConfs1 = ArrayAdapter.createFromResource(this, R.array.confirmations, android.R.layout.simple_spinner_item);
        mAdapterModeConfs1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        mAdapterModeConfs2 = ArrayAdapter.createFromResource(this, R.array.confirmations_mini, android.R.layout.simple_spinner_item);
        mAdapterModeConfs2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        // Кнопка #1
        mEditLabel1 = (EditText)findViewById(R.id.editLabel1);
        mSpinnerConfirmations1 = (Spinner)findViewById(R.id.spinnerConfirmations1);
        
        // Кнопка #2
        mEditLabel2 = (EditText)findViewById(R.id.editLabel2);
        mSpinnerConfirmations2 = (Spinner)findViewById(R.id.spinnerConfirmations2);
                        
	    // Кнопка "Далее"
	    mButtonKeySetsNext = (Button)findViewById(R.id.buttonKeySetsNext);
	    mButtonKeySetsNext.setOnClickListener(mButtonKeySetsNextClick);
	    	    
	    // Ставим начальные значения
	    if (savedInstanceState != null) {
	    	mSpinnerModesConfs.setSelection(savedInstanceState.getInt("spinner_modes_confs_position"), false);
	    	
	    	mConfirmation1 = savedInstanceState.getInt("confirmation_1");
	    	mConfirmation2 = savedInstanceState.getInt("confirmation_2");
	    }
	    else {
	    	mSpinnerModesConfs.setSelection(mSettings.getInt(MyDevice.APP_PREFS_MODE_CONFS, 0), false);
	    	mEditLabel1.setText(mSettings.getString(MyDevice.APP_PREFS_LABEL_1, ""));
    		mEditLabel2.setText(mSettings.getString(MyDevice.APP_PREFS_LABEL_2, ""));
    		
    		mConfirmation1 = mSettings.getInt(MyDevice.APP_PREFS_CONFIRMATION_1, 0);
    		mConfirmation2 = mSettings.getInt(MyDevice.APP_PREFS_CONFIRMATION_2, 0);
	    }
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if (!mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {
			mButtonKeySetsNext.setText(R.string.save);
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		
		savedInstanceState.putInt("spinner_modes_confs_position", mSpinnerModesConfs.getSelectedItemPosition());
		savedInstanceState.putInt("confirmation_1", mSpinnerConfirmations1.getSelectedItemPosition());
		savedInstanceState.putInt("confirmation_2", mSpinnerConfirmations2.getSelectedItemPosition());
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмник
		unregisterReceiver(mWizardReceiver);
		
		// Освобождаем звук
		releaseSound();
	}
	
	
	
    // Проигрывание звука
    private void playSound(int res) {
    	releaseSound();
    	
    	mMediaPlayer = MediaPlayer.create(this, res);    	
    	if (mMediaPlayer != null) {
    		mMediaPlayer.start();
    	}
    }
    
    // Освобождение звуков
    private void releaseSound() {
    	if (mMediaPlayer != null) {
    		mMediaPlayer.release();
    		mMediaPlayer = null;
    	}
    }
    
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}
