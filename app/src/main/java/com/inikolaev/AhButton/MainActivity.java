package com.inikolaev.AhButton;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements AsyncTaskFragment.AsyncTaskCallbacks {

	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private Button mButtonAlarm1;
	private Button mButtonAlarm2;
	private TextView mTextAlarmDesc;
	private Button mButtonAlarmCancel;
	private Button mButtonGeolocation;	
	private Button mButtonProfile;
	private Button mButtonDevice;
	private Button mButtonPay;
	private Button mButtonAbout;
	
	private int mAlarmId = 0;
	private int mButtonId = 0;
	
	// Слушатель нажатия на кнопки #1, 2, 3
	private final OnClickListener mButtonAlarmNClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			int buttonId = 0;
			
			switch (v.getId()) {
				case R.id.buttonAlarm1:
					buttonId = 1;
					break;
				case R.id.buttonAlarm2:
					buttonId = 2;
					break;
			}
			
			// Шлём сигнал тревоги
			Intent intent = new Intent(AlarmService.ACTION_ALARM_SENT);
			intent.putExtra("button_id", buttonId);
	        sendBroadcast(intent);
		}
	};
	
	// Приёмник событий тревоги
	private final BroadcastReceiver mAlarmReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_ALARM_STARTED.equals(action)) {
				int alarmId = intent.getIntExtra("alarm_id", 0);
				int buttonId = intent.getIntExtra("button_id", 0);
				
			    if (alarmId > 0 && buttonId > 0) {
			    	mAlarmId = alarmId;
			    	mButtonId = buttonId;
			    	
			    	startAlarmMode();
			    }
			}
			else if (AlarmService.ACTION_ALARM_FINISHED.equals(action)) {
				mAlarmId = 0;
				mButtonId = 0;
				
				finishAlarmMode();
			}
		}
	};
	
	// Приёмник события закрытия окна
	private final BroadcastReceiver mStopReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_APP_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку отмены тревоги
	private final OnClickListener mButtonAlarmCancelClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(getApplicationContext(), AlarmCancelActivity.class);
			intent.putExtra("alarm_id", mAlarmId);
			startActivity(intent);
		}
	};
	
	// Слушатель нажатия на кнопку местоположения
	private final OnClickListener mButtonGeolocationClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(getApplicationContext(), GeolocationActivity.class);
			intent.putExtra("alarm_id", mAlarmId);
			startActivity(intent);
		}
	};
	
	// Слушатель нажатия на кнопку "Профиль"
	@SuppressLint("InflateParams")
	private final OnClickListener mButtonProfileClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			final EditText input = (EditText)LayoutInflater.from(MainActivity.this).inflate(R.layout.edit_text_password, null);
			
			final AlertDialog passwordDialog = new AlertDialog.Builder(MainActivity.this)
        	.setMessage("Для доступа к разделу введите ваш пароль:")
        	.setCancelable(true)
        	.setView(input)
        	.setPositiveButton("Подтвердить", null)
        	.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
        		public void onClick(DialogInterface dialog, int id) {
        			dialog.dismiss();
        		}
        	})
        	.create();
		
			passwordDialog.show();
			
			passwordDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
                	if (input.length() < 5) {
        				Toast.makeText(getApplicationContext(), "Длина пароля должна быть не менее 5 символов.", Toast.LENGTH_SHORT).show();
        				return;
        			}
                	
        			if (MyDevice.hasInternetConnection(getApplicationContext())) {
        				HashMap<String, String> params = new HashMap<String, String>();
        								
        				params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
        				params.put("password", input.getText().toString());
        				params.put("imei", MyDevice.getImei(getApplicationContext()));
        				params.put("_to", "profile");
        				
        				mAsyncTaskFragment.execute("user_login", params, null);
        			}
        			else {
        				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
        			}
        			
					passwordDialog.dismiss();
				}
			});
		}
	};
	
	// Слушатель нажатия на кнопку "Брелок"
	@SuppressLint("InflateParams")
	private final OnClickListener mButtonDeviceClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			final EditText input = (EditText)LayoutInflater.from(MainActivity.this).inflate(R.layout.edit_text_password, null);
			
			final AlertDialog passwordDialog = new AlertDialog.Builder(MainActivity.this)
        	.setMessage("Для доступа к разделу введите ваш пароль:")
        	.setCancelable(true)
        	.setView(input)
        	.setPositiveButton("Подтвердить", null)
        	.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
        		public void onClick(DialogInterface dialog, int id) {
        			dialog.dismiss();
        		}
        	})
        	.create();
		
			passwordDialog.show();
			
			passwordDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (input.length() < 5) {
        				Toast.makeText(getApplicationContext(), "Длина пароля должна быть не менее 5 символов.", Toast.LENGTH_SHORT).show();
        				return;
        			}
                	
                	if (MyDevice.hasInternetConnection(getApplicationContext())) {
        				HashMap<String, String> params = new HashMap<String, String>();
        								
        				params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
        				params.put("password", input.getText().toString());
        				params.put("imei", MyDevice.getImei(getApplicationContext()));
        				params.put("_to", "settings");
        				
        				mAsyncTaskFragment.execute("user_login", params, null);
        			}
        			else {
        				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
        			}
                	
                	passwordDialog.dismiss();
				}
			});
		}
	};
	
	// Слушатель нажатия на кнопку "Оплата"
	private final OnClickListener mButtonPayClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// Запрашиваем баланс на сервере
			if (MyDevice.hasInternetConnection(getApplicationContext())) {
				HashMap<String, String> params = new HashMap<String, String>();
								
				params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
				params.put("imei", MyDevice.getImei(getApplicationContext()));
				params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
				params.put("_to", "pay");
				
				mAsyncTaskFragment.execute("user_balance", params, null);
			}
			else {
				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "О программе"
	private final OnClickListener mButtonAboutClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(new Intent(getApplicationContext(), AboutActivity.class));
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.getParam("_to").equals("pay")) {
    		if (http.isSuccess()) {
    			if (http.getInt("status") > 0) {
    				String status = "";
    				
    				if ((http.getString("status")).equals("1")) {
    					status = "действующий";
    				}
    				else if ((http.getString("status")).equals("2")) {
    					status = "заморожен";
    				}
    				else if ((http.getString("status")).equals("3")) {
    					status = "заблокирован";
    				}
    				
    				mProgressDialog.dismiss();
    				
    				Intent intent = new Intent(this, PayActivity.class);
    				intent.putExtra("ls", http.getString("ls"));
    				intent.putExtra("status", status);
    				intent.putExtra("balance", http.getString("balance"));
    				
    				startActivity(intent);
    			}
    			else {
    				mProgressDialog.dismiss();
    				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
    			}
    		}
    		else {
    			mProgressDialog.dismiss();
    			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
    		}
		}	
		else if (http.getParam("_to").equals("profile")) {
    		if (http.isSuccess()) {
    			if (http.getInt("status") > 0) {
    				mProgressDialog.dismiss();
    				
    				Intent intent = new Intent(this, ProfileActivity.class);
    				//intent.putExtra("last_name", http.getString("last_name"));
    				//intent.putExtra("first_name", http.getString("first_name"));
    				//intent.putExtra("patr_name", http.getString("patr_name"));
    				//intent.putExtra("birth_date", http.getString("birth_date"));
    				intent.putExtra("phone", http.getString("phone"));
    				intent.putExtra("codeword", http.getString("codeword"));
    				intent.putExtra("email", http.getString("email"));
    				//intent.putExtra("city", http.getString("city"));
    				//intent.putExtra("street", http.getString("street"));
    				//intent.putExtra("house", http.getString("house"));
    				//intent.putExtra("building", http.getString("building"));
    				//intent.putExtra("apartment", http.getString("apartment"));
    				//intent.putExtra("passport_ser_num", http.getString("passport_ser_num"));
    				//intent.putExtra("passport_issued_by", http.getString("passport_issued_by"));
    				//intent.putExtra("passport_issue_date", http.getString("passport_issue_date"));
    				intent.putExtra("passport_photo_status", http.getInt("passport_photo_status"));
    				intent.putExtra("passport_photo_url", http.getString("passport_photo_url"));
    				intent.putExtra("passport_photo_reg_status", http.getInt("passport_photo_reg_status"));
    				intent.putExtra("passport_photo_reg_url", http.getString("passport_photo_reg_url"));
    				intent.putExtra("guarded_last_name", http.getString("guarded_last_name"));
    				intent.putExtra("guarded_first_name", http.getString("guarded_first_name"));
    				intent.putExtra("guarded_patr_name", http.getString("guarded_patr_name"));
    				intent.putExtra("guarded_birth_date", http.getString("guarded_birth_date"));
    				intent.putExtra("guarded_phone", http.getString("guarded_phone"));
    				//intent.putExtra("guarded_city", http.getString("guarded_city"));
    				//intent.putExtra("guarded_street", http.getString("guarded_street"));
    				//intent.putExtra("guarded_house", http.getString("guarded_house"));
    				//intent.putExtra("guarded_building", http.getString("guarded_building"));
    				//intent.putExtra("guarded_apartment", http.getString("guarded_apartment"));
    				intent.putExtra("guarded_phone_contact", http.getString("guarded_phone_contact"));
    				intent.putExtra("guarded_deseases", http.getString("guarded_deseases"));
    				intent.putExtra("guarded_photo_status", http.getInt("guarded_photo_status"));
    				intent.putExtra("guarded_photo_url", http.getString("guarded_photo_url"));
    				
    				startActivity(intent);
    			}
    			else {
    				mProgressDialog.dismiss();
    				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
    			}
    		}
    		else {
    			mProgressDialog.dismiss();
    			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
    		}
		}	
		else if (http.getParam("_to").equals("settings")) {
    		if (http.isSuccess()) {
    			if (http.getInt("status") > 0) {
    				mProgressDialog.dismiss();
    				startActivity(new Intent(this, SettingsActivity.class));
    			}
    			else {
    				mProgressDialog.dismiss();
    				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
    			}
    		}
    		else {
    			mProgressDialog.dismiss();
    			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
    		}
		}
	}
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_main);
	    
	    // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        // Регистрируем приёмник событий AlarmService
        IntentFilter filter = new IntentFilter();
        filter.addAction(AlarmService.ACTION_ALARM_STARTED);
        filter.addAction(AlarmService.ACTION_ALARM_FINISHED);
        registerReceiver(mAlarmReceiver, filter);
        
	    // Регистрируем приёмник события закрытия окна
        registerReceiver(mStopReceiver, new IntentFilter(AlarmService.ACTION_APP_TERMINATED));
	    
	    // Получаем размеры экрана
        int[] screenSizes = MyDevice.getScreenSizes(this);
	    int screenWidth = screenSizes[0];
	    int screenHeight = screenSizes[1];
	    
        // Кнопка #1
	    mButtonAlarm1 = (Button)findViewById(R.id.buttonAlarm1);
	    mButtonAlarm1.setOnClickListener(mButtonAlarmNClick);
	    
	    LayoutParams button1Params = new LayoutParams(Math.round(screenWidth/100*40), Math.round(screenHeight/100*35));
	    button1Params.rightMargin = MyDevice.dpToPixels(this, 10);
	    mButtonAlarm1.setLayoutParams(button1Params);
	    mButtonAlarm1.setPadding(0, Math.round(screenHeight/100*16), 0, 0);
	    
	    // Кнопка #2
	    mButtonAlarm2 = (Button)findViewById(R.id.buttonAlarm2);
	    mButtonAlarm2.setOnClickListener(mButtonAlarmNClick);
	    mButtonAlarm2.setLayoutParams(new LayoutParams(Math.round(screenWidth/100*40), Math.round(screenHeight/100*35)));
	    mButtonAlarm2.setPadding(0, Math.round(screenHeight/100*16), 0, 0);
	    
	    // Вывод текста службы тревоги
	    mTextAlarmDesc = (TextView)findViewById(R.id.textAlarmDesc);
	    
	    // Кнопка отмены тревоги
	    mButtonAlarmCancel = (Button)findViewById(R.id.buttonAlarmCancel);
	    mButtonAlarmCancel.setOnClickListener(mButtonAlarmCancelClick);
	    mButtonAlarmCancel.setLayoutParams(new LayoutParams(Math.round(screenWidth/100*80), Math.round(screenHeight/100*60)));
	    
	    StateListDrawable background = (StateListDrawable)mButtonAlarmCancel.getBackground();
	    AnimationDrawable btnAnimation = (AnimationDrawable)background.getCurrent();
	    btnAnimation.setVisible(true, true);
	    
	    // Кнопка уточнения местоположения
	    mButtonGeolocation = (Button)findViewById(R.id.buttonGeolocation);
	    mButtonGeolocation.setOnClickListener(mButtonGeolocationClick);

	    // Кнопка "Профиль"
	    mButtonProfile = (Button)findViewById(R.id.buttonProfile);
	    mButtonProfile.setOnClickListener(mButtonProfileClick);
	    
	    // Кнопка "Брелок"
	    mButtonDevice = (Button)findViewById(R.id.buttonDevice);
	    mButtonDevice.setOnClickListener(mButtonDeviceClick);
	    
	    // Кнопка "Оплата"
	    mButtonPay = (Button)findViewById(R.id.buttonPay);
	    mButtonPay.setOnClickListener(mButtonPayClick);
	    
	    // Кнопка "О программе"
	    mButtonAbout = (Button)findViewById(R.id.buttonAbout);
	    mButtonAbout.setOnClickListener(mButtonAboutClick);
	}
	
    @Override
    public void onResume() {
    	super.onResume();
    	
	    // Сброс на экран запуска
    	if (mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false) || mSettings.getString(MyDevice.APP_PREFS_LOGIN, "").equals("")) {
    		startActivity(new Intent(this, SplashActivity.class));
    		finish();
    	}
    	else {
    		// Запуск службы AlarmService, если не запущена
    		if (!MyDevice.isAlarmServiceRunning(this)) {
    			Intent intent = new Intent(this, AlarmService.class);
    			intent.putExtra("no_silent", true);
    			startService(intent);
    		}
    		else {
    			sendBroadcast(new Intent(AlarmService.ACTION_ALARM_REQUESTED));
    		}
    		
    		mButtonAlarm1.setText(mSettings.getString(MyDevice.APP_PREFS_LABEL_1, ""));
			mButtonAlarm2.setText(mSettings.getString(MyDevice.APP_PREFS_LABEL_2, ""));
    	}
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
    	super.onWindowFocusChanged(hasFocus);
    	
		StateListDrawable background = (StateListDrawable)mButtonAlarmCancel.getBackground();
    	Drawable current = background.getCurrent();
    	if (current instanceof AnimationDrawable) {
    		AnimationDrawable btnAnimation = (AnimationDrawable)current;
    		btnAnimation.start();
    	}
    }
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмники
		unregisterReceiver(mAlarmReceiver);
		unregisterReceiver(mStopReceiver);
	}
	
	
	
	// Переход в режим тревоги
	private void startAlarmMode() {
		switch (mButtonId) {
			case 1:
				mTextAlarmDesc.setText(mSettings.getString(MyDevice.APP_PREFS_LABEL_1, ""));
				break;
			case 2:
				mTextAlarmDesc.setText(mSettings.getString(MyDevice.APP_PREFS_LABEL_2, ""));
				break;
		}
		
        mButtonAlarm1.setVisibility(View.GONE);
		mButtonAlarm2.setVisibility(View.GONE);
		
		mTextAlarmDesc.setVisibility(View.VISIBLE);
		mButtonAlarmCancel.setVisibility(View.VISIBLE);
		mButtonGeolocation.setVisibility(View.VISIBLE);
		
		mButtonProfile.setVisibility(View.GONE);
		mButtonDevice.setVisibility(View.GONE);
		mButtonPay.setVisibility(View.GONE);
		mButtonAbout.setVisibility(View.GONE);
	}
	
	// Переход в обычный режим
	private void finishAlarmMode() {
		mTextAlarmDesc.setText("");
		
		mButtonAlarm1.setVisibility(View.VISIBLE);
		mButtonAlarm2.setVisibility(View.VISIBLE);
		
		mTextAlarmDesc.setVisibility(View.GONE);
		mButtonAlarmCancel.setVisibility(View.GONE);
		mButtonGeolocation.setVisibility(View.GONE);
		
		mButtonProfile.setVisibility(View.VISIBLE);
		mButtonDevice.setVisibility(View.VISIBLE);
		mButtonPay.setVisibility(View.VISIBLE);
		mButtonAbout.setVisibility(View.VISIBLE);
	}
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}