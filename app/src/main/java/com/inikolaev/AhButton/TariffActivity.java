package com.inikolaev.AhButton;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class TariffActivity extends FragmentActivity implements AsyncTaskFragment.AsyncTaskCallbacks {

	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private Spinner mSpinnerTariffs;
	private TextView mTextTariffDesc;
	private Button mButtonTariffNext;
	
	private ArrayAdapter<String> mTariffAdapter;
	
	private ArrayList<String> mTariffIds;
	private ArrayList<String> mTariffNames;
	private ArrayList<String> mTariffDescs;
		
	// Приёмник события закрытия мастера
	private final BroadcastReceiver mWizardReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_WIZARD_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель смены тарифа
	private final OnItemSelectedListener mSpinnerTariffsSelected = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
			mTextTariffDesc.setText(mTariffDescs.get(position));
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {}	
	};
	
	// Слушатель нажатия на кнопку "Далее"
	private final OnClickListener mButtonTariffNextClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (!mSettings.getBoolean(MyDevice.APP_PREFS_TARIFF_SET, false)) {
				// Записываем данные пользователя на сервер
				if (MyDevice.hasInternetConnection(getApplicationContext())) {
					HashMap<String, String> params = new HashMap<String, String>();
					
					params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
					params.put("imei", MyDevice.getImei(getApplicationContext()));
					params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
					params.put("tariff", mTariffIds.get(mSpinnerTariffs.getSelectedItemPosition()));
					
					mAsyncTaskFragment.execute("user_tariff", params, null);
				}
				else {
					Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
				}
			}
			else {
				startActivity(new Intent(getApplicationContext(), EndWizardActivity.class));
			}
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				Editor editor = mSettings.edit();
				editor.putBoolean(MyDevice.APP_PREFS_TARIFF_SET, true);
				editor.apply();
				
				mProgressDialog.dismiss();
				
				startActivity(new Intent(this, EndWizardActivity.class));
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
			}
		}
		else {
			mProgressDialog.dismiss();
			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_tariff);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    if (mAsyncTaskFragment.isTaskRunning()) {
	    	showProgressDialog();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        // Регистрируем приёмник события закрытия мастера
        registerReceiver(mWizardReceiver, new IntentFilter(AlarmService.ACTION_WIZARD_TERMINATED));
        
	    // Список тарифов
	    mSpinnerTariffs = (Spinner)findViewById(R.id.spinnerTariffs);
	    mSpinnerTariffs.setOnItemSelectedListener(mSpinnerTariffsSelected);
	    
	    // Описание тарифов
	    mTextTariffDesc = (TextView)findViewById(R.id.textTariffDesc);
	    
	    // Кнопка "Далее"
	    mButtonTariffNext = (Button)findViewById(R.id.buttonTariffNext);
	    mButtonTariffNext.setOnClickListener(mButtonTariffNextClick);
	    
	    // Подстановка тарифов
	    Intent intent = getIntent();
	    
	    mTariffIds = intent.getStringArrayListExtra("tariff_ids");
	    if (mTariffIds == null) {
	    	mTariffIds = new ArrayList<String>();
	    }
	    
	    mTariffNames = intent.getStringArrayListExtra("tariff_names");
	    if (mTariffNames == null) {
	    	mTariffNames = new ArrayList<String>();
	    }
	    
	    mTariffDescs = intent.getStringArrayListExtra("tariff_descs");
	    if (mTariffDescs == null) {
	    	mTariffDescs = new ArrayList<String>();
	    }
	    
        mTariffAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mTariffNames);
        mTariffAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerTariffs.setAdapter(mTariffAdapter);
        
	    // Ставим начальные значения
        if (savedInstanceState != null) {
        	mSpinnerTariffs.setSelection(savedInstanceState.getInt("spinner_tariff_position"), false);
        	mTextTariffDesc.setText(savedInstanceState.getString("text_tariff_desc"));
        }
        else {
        	mSpinnerTariffs.setSelection(intent.getIntExtra("tariff", 0), false);
	    	mTextTariffDesc.setText(mTariffDescs.get(intent.getIntExtra("tariff", 0)));
        }
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		// Отключаем поле тарифа
	    if (mSettings.getBoolean(MyDevice.APP_PREFS_TARIFF_SET, false)) {
	    	mSpinnerTariffs.setEnabled(false);
	    }
	    else {
	    	mSpinnerTariffs.setEnabled(true);
	    }
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		
		savedInstanceState.putInt("spinner_tariff_position", mSpinnerTariffs.getSelectedItemPosition());
		savedInstanceState.putString("text_tariff_desc", mTextTariffDesc.getText().toString());
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмник
		unregisterReceiver(mWizardReceiver);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}
