package com.inikolaev.AhButton;

public class MyBluetoothDevice {
	public String name;
	public String address;
	public int type;
	
	public MyBluetoothDevice(String name, String address, int type) {
		this.name = name;
		this.address = address;
		this.type = type;
	}
}