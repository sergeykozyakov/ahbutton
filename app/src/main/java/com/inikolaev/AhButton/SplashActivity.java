package com.inikolaev.AhButton;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

public class SplashActivity extends FragmentActivity implements AsyncTaskFragment.AsyncTaskCallbacks {
	
	private static final int SPLASH_DURATION = 500;
	
	private SharedPreferences mSettings;
	private Timer mSplashTimer;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private boolean mIsTimerStarted;
	private boolean mIsBackButtonPressed;
	
	@Override
	public void onPreExecute() {}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				if ((http.getString("status")).equals("1")) {
					double balance = http.getDouble("balance");
					
					if (balance < 30 && balance > 0) {
						Toast.makeText(this, "Ваш баланс близок к нулю! Внесите оплату.", Toast.LENGTH_LONG).show();
					}
				}
				else if ((http.getString("status")).equals("2")) {
					Toast.makeText(this, "Лицевой счёт заморожен! Внесите оплату.", Toast.LENGTH_LONG).show();
				}
				else if ((http.getString("status")).equals("3")) {
					Toast.makeText(this, "Договор расторгнут! Услуга не оказывается.", Toast.LENGTH_LONG).show();
				}
				
				startActivity(new Intent(this, MainActivity.class));
				finish();
			}
			else {
				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
				finish();
			}
		}
		else {
			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
			finish();
		}
	}
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_splash);
	    
	    if (savedInstanceState != null) {
	    	mIsTimerStarted = savedInstanceState.getBoolean("is_timer_started");
	    	mIsBackButtonPressed = savedInstanceState.getBoolean("is_back_button_pressed");
	    }
	    
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
	    
	    if (mIsTimerStarted) {
	    	return;
	    }
	    
	    mSplashTimer = new Timer();
	    mSplashTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				mSplashTimer.cancel();
				
            	if (!mIsBackButtonPressed) {
            		if (mSettings.getBoolean(MyDevice.APP_PREFS_LIC, false) && !mSettings.getBoolean(MyDevice.APP_PREFS_REGISTERING, false)) {
            			if (MyDevice.hasInternetConnection(getApplicationContext())) {
            				HashMap<String, String> params = new HashMap<String, String>();
            								
            				params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
            				params.put("imei", MyDevice.getImei(getApplicationContext()));
            				params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
            				
            				mAsyncTaskFragment.execute("user_balance", params, null);
            			}
            			else {
            				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
            				finish();
            			}
            		}
            		else {
            			startActivity(new Intent(getApplicationContext(), LicenceActivity.class));
            			finish();
            		}            		
            	}
            	else {
            		finish();
            	}
			}
		}, SPLASH_DURATION);
	    
	    mIsTimerStarted = true;
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		
		savedInstanceState.putBoolean("is_timer_started", mIsTimerStarted);
		savedInstanceState.putBoolean("is_back_button_pressed", mIsBackButtonPressed);
	}
	
    @Override
    public void onBackPressed() {
    	super.onBackPressed();
    	
    	mIsBackButtonPressed = true;
    }
}