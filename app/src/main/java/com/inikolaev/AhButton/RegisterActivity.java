package com.inikolaev.AhButton;

import java.util.HashMap;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends FragmentActivity implements AsyncTaskFragment.AsyncTaskCallbacks {

	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private EditText mEditNewLogin;
	private EditText mEditNewPassword;
	private EditText mEditNewPasswordConfirm;
	private Button mButtonAccountHave;
	private Button mButtonRegisterNext;
	
	// Приёмник события закрытия мастера
	private final BroadcastReceiver mWizardReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_WIZARD_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Есть учётная запись"
	private final OnClickListener mButtonAccountHaveClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(new Intent(getApplicationContext(), LoginActivity.class));
		}
	};
	
	// Слушатель нажатия на кнопку "Далее"
	private final OnClickListener mButtonRegisterNextClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// Проверяем введённые данные
			if (mEditNewLogin.length() == 0) {
				Toast.makeText(getApplicationContext(), "Введите логин!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (mEditNewPassword.length() < 5 || mEditNewPasswordConfirm.length() < 5) {
				Toast.makeText(getApplicationContext(), "Длина пароля должна быть не менее 5 символов.", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (!mEditNewPassword.getText().toString().equals(mEditNewPasswordConfirm.getText().toString())) {
				Toast.makeText(getApplicationContext(), "Пароли должны совпадать!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// Отправляем данные о регистрации на сервер
			if (MyDevice.hasInternetConnection(getApplicationContext())) {
				HashMap<String, String> params = new HashMap<String, String>();
								
				params.put("login", mEditNewLogin.getText().toString());
				params.put("password", mEditNewPassword.getText().toString());
				params.put("imei", MyDevice.getImei(getApplicationContext()));
				params.put("accepted", mSettings.getBoolean(MyDevice.APP_PREFS_LIC, false) ? "1" : "0");
				
				mAsyncTaskFragment.execute("user_register", params, null);
			}
			else {
				Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				Editor editor = mSettings.edit();
				editor.putString(MyDevice.APP_PREFS_LOGIN, mEditNewLogin.getText().toString());
				editor.putString(MyDevice.APP_PREFS_PASSWORD, mEditNewPassword.getText().toString());
				editor.apply();
				
				mProgressDialog.dismiss();
				
				startActivity(new Intent(this, ProfileActivity.class));
				finish();
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
			}
		}
		else {
			mProgressDialog.dismiss();
			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
		}
	}
	
		
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_register);
	    
	    // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	    if (mAsyncTaskFragment.isTaskRunning()) {
	    	showProgressDialog();
	    }
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        // Регистрируем приёмник события закрытия мастера
        registerReceiver(mWizardReceiver, new IntentFilter(AlarmService.ACTION_WIZARD_TERMINATED));
	    
	    // Поля ввода
        mEditNewLogin = (EditText)findViewById(R.id.editNewLogin);
        mEditNewPassword = (EditText)findViewById(R.id.editNewPassword);
        mEditNewPasswordConfirm = (EditText)findViewById(R.id.editNewPasswordConfirm);
	    
	    // Кнопка "Есть учётная запись"
	    mButtonAccountHave = (Button)findViewById(R.id.buttonAccountHave);
	    mButtonAccountHave.setOnClickListener(mButtonAccountHaveClick);
	    
	    // Кнопка "Далее"
	    mButtonRegisterNext = (Button)findViewById(R.id.buttonRegisterNext);
	    mButtonRegisterNext.setOnClickListener(mButtonRegisterNextClick);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмник
		unregisterReceiver(mWizardReceiver);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}
