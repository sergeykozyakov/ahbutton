package com.inikolaev.AhButton;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

public class DeviceCheckActivity extends FragmentActivity implements AsyncTaskFragment.AsyncTaskCallbacks {
	
	private static final int TEST_BUTTON_DURATION = 250;

	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private AsyncTaskFragment mAsyncTaskFragment;
	
	private Button mButtonAlarm1Test;
	private Button mButtonAlarm2Test;
	private Button mButtonDeviceTest;
	private Button mButtonServerTest;
	
	// Приёмник событий тестового режима
	private final BroadcastReceiver mTestReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_TEST_SENT.equals(action)) {
				int button = intent.getIntExtra("button", 0);
				
			    switch (button) {
			    	case 1:
			    		Toast.makeText(getApplicationContext(), "Нажата кнопка: #1\nСлужба: " + mSettings.getString(MyDevice.APP_PREFS_LABEL_1, "неизвестно"), Toast.LENGTH_SHORT).show();
			    		mButtonAlarm1Test.setEnabled(true);

			    		new Handler().postDelayed(new Runnable() {
			                @Override
			                public void run() {
			                	mButtonAlarm1Test.setEnabled(false);
			                }
			    		}, TEST_BUTTON_DURATION);
			    		break;
			    		
			    	case 2:
			    		Toast.makeText(getApplicationContext(), "Нажата кнопка: #2\nСлужба: " + mSettings.getString(MyDevice.APP_PREFS_LABEL_2, "неизвестно"), Toast.LENGTH_SHORT).show();
			    		mButtonAlarm2Test.setEnabled(true);
			    		
			    		new Handler().postDelayed(new Runnable() {
			                @Override
			                public void run() {
			                	mButtonAlarm2Test.setEnabled(false);
			                }
			    		}, TEST_BUTTON_DURATION);
			    		break;
			    }
			}
		}
	};
	
	// Приёмник события закрытия окна
	private final BroadcastReceiver mStopReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_APP_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Проверка брелка"
	private final OnClickListener mButtonDeviceTestClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Toast.makeText(getApplicationContext(), "Проверка связи с брелком...", Toast.LENGTH_SHORT).show();
			
			if (MyDevice.isAlarmServiceRunning(getApplicationContext())) {
				Toast.makeText(getApplicationContext(), "Связь с брелком успешо установлена и поддерживается!", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(getApplicationContext(), "Связь с брелком отсутствует!", Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Проверка сервера"
	@SuppressLint("SimpleDateFormat")
	private final OnClickListener mButtonServerTestClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
	    	if (MyDevice.hasInternetConnection(getApplicationContext())) {
	    		Toast.makeText(getApplicationContext(), "Проверка связи с сервером...", Toast.LENGTH_SHORT).show();
	    		
	    		HashMap<String, String> params = new HashMap<String, String>();
	    				
	    		params.put("login", mSettings.getString(MyDevice.APP_PREFS_LOGIN, ""));
	    		params.put("imei", MyDevice.getImei(getApplicationContext()));
	    		params.put("mac", mSettings.getString(MyDevice.APP_PREFS_MAC, "00:00:00:00:00:00"));
	    		params.put("datetime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	    		params.put("action", "2");
	    		params.put("test", "1");
	    		
	    		mAsyncTaskFragment.execute("alarm_send", params, null);
	    	}
	    	else {
	    		Toast.makeText(getApplicationContext(), "Отсутствует соединение с интернет!", Toast.LENGTH_SHORT).show();
	    	}
		}
	};
	
	@Override
	public void onPreExecute() {
		showProgressDialog();
	}

	@Override
	public void onProgressUpdate(Integer... progress) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute(MyHttpClient http) {
		if (http.isSuccess()) {
			if (http.getInt("status") > 0) {
				mProgressDialog.dismiss();
				Toast.makeText(this, "Связь с сервером успешно установлена и поддерживается!", Toast.LENGTH_SHORT).show();
			}
			else {
				mProgressDialog.dismiss();
				Toast.makeText(this, http.getString("error"), Toast.LENGTH_SHORT).show();
			}
		}
		else {
			mProgressDialog.dismiss();
			Toast.makeText(this, "Невозможно соединиться с сервером!", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_device_check);
	    
	    if (Build.VERSION.SDK_INT >= 9) {
	    	getActionBar().setDisplayHomeAsUpEnabled(true);
	    }
	    
	    // Подключаем фрагмент для потоков
	    FragmentManager fm = getSupportFragmentManager();
	    mAsyncTaskFragment = (AsyncTaskFragment)fm.findFragmentByTag("task_fragment");
	    
	    if (mAsyncTaskFragment == null) {
	    	mAsyncTaskFragment = AsyncTaskFragment.newInstance();
			fm.beginTransaction().add(mAsyncTaskFragment, "task_fragment").commit();
	    }
	    
	   // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
		// Регистрируем приёмник событий тестового режима
        registerReceiver(mTestReceiver, new IntentFilter(AlarmService.ACTION_TEST_SENT));
        
	    // Регистрируем приёмник события закрытия окна
	    registerReceiver(mStopReceiver, new IntentFilter(AlarmService.ACTION_APP_TERMINATED));
	    
	    // Получаем размеры экрана
        int[] screenSizes = MyDevice.getScreenSizes(this);
	    int screenWidth = screenSizes[0];
	    int screenHeight = screenSizes[1];
        
	    // Кнопка #1
	    mButtonAlarm1Test = (Button)findViewById(R.id.buttonAlarm1Test);
	    mButtonAlarm1Test.setText(mSettings.getString(MyDevice.APP_PREFS_LABEL_1, ""));
	    mButtonAlarm1Test.setEnabled(false);
	    
	    LayoutParams button1Params = new LayoutParams(Math.round(screenWidth/100*40), Math.round(screenHeight/100*35));
	    button1Params.rightMargin = MyDevice.dpToPixels(this, 10);
	    mButtonAlarm1Test.setLayoutParams(button1Params);
	    mButtonAlarm1Test.setPadding(0, Math.round(screenHeight/100*16), 0, 0);
	    
	    // Кнопка #2
	    mButtonAlarm2Test = (Button)findViewById(R.id.buttonAlarm2Test);
	    mButtonAlarm2Test.setText(mSettings.getString(MyDevice.APP_PREFS_LABEL_2, ""));
	    mButtonAlarm2Test.setEnabled(false);
	    mButtonAlarm2Test.setLayoutParams(new LayoutParams(Math.round(screenWidth/100*40), Math.round(screenHeight/100*35)));
	    mButtonAlarm2Test.setPadding(0, Math.round(screenHeight/100*16), 0, 0);
	    
	    // Кнопка тестирования брелка
	    mButtonDeviceTest = (Button)findViewById(R.id.buttonDeviceTest);
	    mButtonDeviceTest.setOnClickListener(mButtonDeviceTestClick);
	    
	    // Кнопка тестирования сервера
	    mButtonServerTest = (Button)findViewById(R.id.buttonServerTest);
	    mButtonServerTest.setOnClickListener(mButtonServerTestClick);
	    
	    // Шлём сигнал запуска тестового режима
	    sendBroadcast(new Intent(AlarmService.ACTION_TEST_STARTED));
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (Build.VERSION.SDK_INT >= 9) {
			switch (item.getItemId()) {
	    		case android.R.id.home:
	    			onBackPressed();
	    			return true;
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
	    // Шлём сигнал остановки тестового режима
	    sendBroadcast(new Intent(AlarmService.ACTION_TEST_FINISHED));
		
		// Отключаем приёмники
	    unregisterReceiver(mTestReceiver);
		unregisterReceiver(mStopReceiver);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}
