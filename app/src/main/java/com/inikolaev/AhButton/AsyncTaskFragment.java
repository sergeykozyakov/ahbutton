package com.inikolaev.AhButton;

import java.util.HashMap;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class AsyncTaskFragment extends Fragment {

	private AsyncTaskCallbacks mCallbacks;
	private HttpTask mTask;
	
	public static interface AsyncTaskCallbacks {
		void onPreExecute();
		void onProgressUpdate(Integer... progress);
		void onCancelled();
		void onPostExecute(MyHttpClient http);
	}
	
	public static AsyncTaskFragment newInstance() {
        return new AsyncTaskFragment();
    }
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		mCallbacks = (AsyncTaskCallbacks)activity;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setRetainInstance(true);
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		
		mCallbacks = null;
	}
	
	public void execute(Object... params) {
		mTask = new HttpTask();
		mTask.execute(params);
	}
	
	public boolean isTaskRunning() {
		if (mTask != null) {
			if (!mTask.isCancelled() && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				return true;
			}
		}
		return false;
	}
	
	private class HttpTask extends AsyncTask<Object, Integer, MyHttpClient> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if (mCallbacks != null) {
				mCallbacks.onPreExecute();
			}
		}
		
		@SuppressWarnings("unchecked")
		@Override
		protected MyHttpClient doInBackground(Object... params) {
			MyHttpClient http = new MyHttpClient();
			
			http.setUploadProgressCallback(mmUploadProgressCallback);
			http.doPost((String)params[0], (HashMap<String, String>)params[1], (HashMap<String, byte[]>)params[2]);
			
			return http;
		}
		
	    @Override
	    protected void onProgressUpdate(Integer... progress) {
	    	super.onProgressUpdate(progress);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onProgressUpdate(progress);
	    	}
	    }
	    
	    @Override
	    protected void onCancelled() {
	    	super.onCancelled();
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onCancelled();
	    	}
	    }
	    
	    @Override
	    protected void onPostExecute(MyHttpClient http) {
	    	super.onPostExecute(http);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onPostExecute(http);
	    	}
	    }
	    
	    private final ProgressHttpEntityWrapper.ProgressCallback mmUploadProgressCallback = new ProgressHttpEntityWrapper.ProgressCallback() {
			@Override
			public void progress(float progress) {
				int value = Math.round(progress);
				publishProgress(value);
			}
		};
	}
}