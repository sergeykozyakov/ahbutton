package com.inikolaev.AhButton;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;

public class LicenceActivity extends Activity {

	private SharedPreferences mSettings;
	
	private WebView mWebViewLicense;
	
	private Button mButtonAccept;
	private Button mButtonDecline;
	
	// Приёмник события закрытия мастера
	private final BroadcastReceiver mWizardReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (AlarmService.ACTION_WIZARD_TERMINATED.equals(action)) {
				finish();
			}
		}
	};
	
	// Слушатель нажатия на кнопку "Принять"
	private final OnClickListener mButtonAcceptClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Editor editor = mSettings.edit();
			editor.putBoolean(MyDevice.APP_PREFS_LIC, true);
			editor.putBoolean(MyDevice.APP_PREFS_REGISTERING, true);
			editor.apply();
			
			startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
			finish();
		}
	};
	
	// Слушатель нажатия на кнопку "Отклонить"
	private final OnClickListener mButtonDeclineClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Editor editor = mSettings.edit();
			editor.putBoolean(MyDevice.APP_PREFS_LIC, false);
			editor.putBoolean(MyDevice.APP_PREFS_REGISTERING, false);
			editor.apply();
			
			finish();
		}
	};
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_licence);
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        // Регистрируем приёмник события закрытия мастера
        registerReceiver(mWizardReceiver, new IntentFilter(AlarmService.ACTION_WIZARD_TERMINATED));
	    
        // Web-форма
        mWebViewLicense = (WebView)findViewById(R.id.webViewLicense);
        mWebViewLicense.loadUrl("file:///android_asset/oferta.html");
        mWebViewLicense.getSettings().setBuiltInZoomControls(true);
        
	    // Кнопка принятия лицензии
	    mButtonAccept = (Button)findViewById(R.id.buttonAccept);
	    mButtonAccept.setOnClickListener(mButtonAcceptClick);
	    
	    // Кнопка отклонения лицензии
	    mButtonDecline = (Button)findViewById(R.id.buttonDecline);
	    mButtonDecline.setOnClickListener(mButtonDeclineClick);
	}
	
	@Override 
    public void onDestroy() {
		super.onDestroy();
		
		// Отключаем приёмник
		unregisterReceiver(mWizardReceiver);
	}
}
